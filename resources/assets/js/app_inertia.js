window.swal = require("sweetalert2")
window.Swal = window.swal
window.moment = require("moment")


try {
  window.$ = window.jQuery = require("jquery");
  window.ClipboardJS = require("clipboard");

  require("bootstrap");
} catch (e) {
}


import { App, plugin } from "@inertiajs/inertia-vue"
import VeeValidate from "vee-validate"
import VeeValidateEs from "./utilities/vee-validate-es"
import Vue from "vue"
import "./filters/filters"
import FlashMessages from "./components/common/FlashMessages"
import VModal from "vue-js-modal/dist/index.nocss.js"
import "vue-js-modal/dist/styles.css"

Vue.component("FlashMessages", FlashMessages)
Vue.use(VModal)
Vue.use(plugin)
Vue.use(VeeValidate, {
  locale: "es",
  dictionary: {
    es: { messages: VeeValidateEs }
  }
})

Vue.prototype.$route = (...args) => route(...args).url()

const app = document.getElementById("app")

new Vue({
  render: h => h(App, {
    props: {
      initialPage: JSON.parse(app.dataset.page),
      resolveComponent: name => require(`./Pages/${ name }`).default,
    },
  }),
}).$mount(app)
