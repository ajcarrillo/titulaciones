window._ = require("lodash");
window.swal = require("sweetalert2");
window.moment = require("moment")
window.axios = require("axios");

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

try {
  window.$ = window.jQuery = require("jquery");
  window.ClipboardJS = require("clipboard");

  require("bootstrap");
} catch (e) {
}

let token = document.head.querySelector("meta[name=\"csrf-token\"]");
let apiToken = document.head.querySelector("meta[name=\"api-token\"]");

if (token) {
  window.axios.defaults.headers.common["X-CSRF-TOKEN"] = token.content;
} else {
  console.error("CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token");
}

if (apiToken) {
    window.axios.defaults.headers.common['Authorization'] = 'Bearer ' + apiToken.content;
} else {
    console.warn("Check api token");
}

let monthsShortDot = "ENE._FEB._MAR._ABR._MAY._JUN._JUL._AGO._SEP._OCT._NOV._DIC.".split("_"),
    monthsShort    = "ENE_FEB_MAR_ABR_MAY_JUN_JUL_AGO_SEP_OCT_NOV_DIC".split("_");

moment.updateLocale("es", {
  months: "Enero_Febrero_Marzo_Abril_Mayo_Junio_Julio_Agosto_Septiembre_Octubre_Noviembre_Diciembre".split("_"),
  weekdays: "Domingo_Lunes_Martes_Miércoles_Jueves_Viernes_Sábado".split("_"),
  monthsShort: function (m, format) {
    if (!m) {
      return monthsShortDot;
    } else if (/-MMM-/.test(format)) {
      return monthsShort[m.month()];
    } else {
      return monthsShortDot[m.month()];
    }
  },
})
