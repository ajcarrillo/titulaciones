import UsuariosIndex from "../views/users/Index"
import UsuariosCreate from "../views/users/Create"
import UsuariosEdit from "../views/users/Edit"

export default {
  mode: "history",
  routes: [
    {
      path: "/home",
      name: "home"
    },
    {
      path: "/administracion/usuarios",
      name: "users.index",
      component: UsuariosIndex,
    },
    {
      path: "/administracion/usuarios/nuevo",
      name: "usuarios.create",
      component: UsuariosCreate
    },
    {
      path: "/administracion/usuarios/:userId/editar",
      name: "usuarios.edit",
      component: UsuariosEdit,
      props: true
    }
  ]
}
