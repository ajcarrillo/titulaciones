/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");

window.Vue = require("vue");

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

import VeeValidate from "vee-validate"
import VeeValidateEs from "./utilities/vee-validate-es"
import router from "./router"
import "./filters/filters"

Vue.use(VeeValidate, {
  locale: "es",
  dictionary: {
    es: { messages: VeeValidateEs }
  }
})

const app = new Vue({
  router
}).$mount("#app");
