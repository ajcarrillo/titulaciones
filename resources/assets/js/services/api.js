import axios from "axios"

axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

let apiToken = document.head.querySelector("meta[name=\"api-token\"]");

if (apiToken) {
  axios.defaults.headers.common["Authorization"] = "Bearer " + apiToken.content;
} else {
  console.warn("Check api token");
}

export default axios;
