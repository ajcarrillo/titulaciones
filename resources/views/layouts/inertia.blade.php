<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		@auth
			<meta name="api-token" content="{{ Auth::user()->api_token }}">
		@endauth
		<title>{{ config('app.name', 'Laravel') }}</title>

		<link rel="stylesheet" href="{{ mix('css/app.css') }}">
		<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">
		<script src="{{ mix('js/app_inertia.js') }}" defer></script>
		@routes
		<style>
            .navbar-itch .navbar-brand {
                color: white !important;
                font-family: 'Roboto', sans-serif;
                font-weight: 400;
                text-transform: uppercase;
                font-size: 1.25rem;
            }

            .navbar-itch .navbar-nav .nav-link,
            .navbar-submenu .nav-link {
                color: white !important;
                font-family: 'Roboto', sans-serif;
                font-weight: 500;
                letter-spacing: .0892857143em;
                font-size: 0.875rem;
                text-transform: uppercase;
            }

            .navbar-itch .navbar-nav .nav-link:hover {
                background-color: #4361B5;
                border-radius: 5px;
            }

            .navbar-submenu .nav-link:hover {
                background-color: #31ACEC;
                border-radius: 5px;
            }

            .navbar-toggler {
                border-color: white !important;
            }

            .navbar-toggler-icon {
                background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' width='30' height='30' viewBox='0 0 30 30'%3e%3cpath stroke='rgba%28255, 255, 255, 1%29' stroke-linecap='round' stroke-miterlimit='10' stroke-width='2' d='M4 7h22M4 15h22M4 23h22'/%3e%3c/svg%3e") !important;
            }
		</style>
	</head>
	<body>
		<nav class="navbar navbar-light navbar-expand-lg navbar-itch" style="background-color: #0D47A1">
			<a class="navbar-brand" href="{{ url('/') }}">
				{{ config('app.name', 'Laravel') }}
			</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<!-- Left Side Of Navbar -->
				<ul class="navbar-nav mr-auto">

				</ul>

				<!-- Right Side Of Navbar -->
				@include('partials._right_navbar')
			</div>
		</nav>
		<main class="py-4">
			@auth
				<script>
                  window.Laravel = <?php echo json_encode([
                        'roles' => Auth::user()->roles()->pluck('descripcion'),
                    ]); ?>
				</script>
			@endauth
			@inertia
		</main>
	</body>
</html>
