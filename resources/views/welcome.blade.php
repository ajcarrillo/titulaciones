<!doctype html>
<html lang="{{ app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<title>División de Estudios - ITCH</title>

		<!-- Fonts -->
		<link href="https://fonts.googleapis.com/css?family=Raleway:100,300,400,700" rel="stylesheet" type="text/css">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">

		<!-- Styles -->
		<style>
			html, body {
				background-color: #00358B;
				color: #FEFEFE;
				-webkit-font-smoothing: antialiased !important;
				font-family: 'Raleway', sans-serif;
				height: 100vh;
				margin: 0;
			}

			body {
				border-top: 5px solid #EF6730;
			}

			.full-height {
				height: 100vh;
			}

			.flex-center {
				align-items: center;
				display: flex;
				justify-content: center;
			}

			.position-ref {
				position: relative;
			}

			.top-right {
				position: absolute;
				right: 10px;
				top: 18px;
			}

			.content {
				text-align: center;
			}

			.title {
				font-size: 84px;
			}

			.links > a {
				color: #FEFEFE;
				padding: 0 25px;
				font-size: .75rem;
				font-weight: 600;
				letter-spacing: .1rem;
				text-decoration: none;
				text-transform: uppercase;
			}

			.m-b-md {
				margin-bottom: 30px;
			}

			.bg-white {
				background-color: #F2F1EF;
				color: #636b6f;
			}

			.font-thin {
				font-weight: 100;
			}

			.font-light {
				font-weight: 300;
			}

			.font-regular {
				font-weight: 400;
			}

			.font-bold {
				font-weight: 700;
			}
		</style>
	</head>
	<body>
		<div class="flex-center position-ref">
			@if (Route::has('login'))
				<div class="top-right links">
					@auth
						<a href="{{ url('/home') }}">Home</a>
					@else
						<a href="{{ route('login') }}">Login</a>
						<a href="{{ route('register') }}">Register</a>
					@endauth
				</div>
			@endif

			{{--<div class="content">
				<div class="title m-b-md">
					Instituto Tecnológico de Chetumal
				</div>

				<div class="links">
					<a href="https://laravel.com/docs">Documentation</a>
					<a href="https://laracasts.com">Laracasts</a>
					<a href="https://laravel-news.com">News</a>
					<a href="https://forge.laravel.com">Forge</a>
					<a href="https://github.com/laravel/laravel">GitHub</a>
				</div>
			</div>--}}

		</div>
		<section class="main full-height d-flex align-items-center mx-5">
			<div class="logo">
				<img src="{{ asset('img/logo_tec.png') }}" class="img-fluid">
			</div>
			<div class="info align-self-start pl-5" style="padding-top: 4rem">
				<h1>Instituto Tecnológico de Chetumal</h1>
				<h2>División de Estudios Profesionales</h2>
			</div>

		</section>
		<div class="container-fluid bg-white">
			<div class="container">
				<div class="row">
					<div class="col">
						<h1 class="font-light text-center my-3" style="font-size: 3rem">Titulaciones</h1>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<h2 class="font-regular text-center">Generación 2004-2009</h2>
						<div class="row">
							<div class="col">
								<div class="card" style="border: none; box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2)">
									<h5 class="card-header font-bold" style="border:none; background-color: #fff">Opciones de Titulación</h5>
									<div class="card-body" style="background-color: #F9FBFD; border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);">
										<ul>
											<li>TESIS PROFESIONAL</li>
											<li>PROYECTO DE INVESTIGACIÓN</li>
											<li>EXAMEN GLOBAL POR ÁREAS DE CONOCIMIENTO</li>
											<li>ESCOLARIDAD POR PROMEDIO</li>
											<li>MEMORIA DE RESIDENCIA PROFESIONAL</li>
											<li>TITULACIÓN INTEGRADA</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="card" style="border: none; box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2)">
									<h5 class="card-header font-bold" style="border:none; background-color: #fff">Requisitos para obtener el Título Profesional</h5>
									<div class="card-body" style="background-color: #F9FBFD; border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);">
										<ul>
											<li>Aprobar todas las asignaturas.</li>
											<li>Haber realizado el Servicio Social.</li>
											<li>Concluir la Residencia Profesional.</li>
											<li>Solicitar la aprobación de la opción elegida.</li>
											<li>No tener adeudo económico en el Instituto Tecnológico del cual egresó.</li>
											<li>Haber demostrado la capacidad de comprender artículos técnico-científicos de su especialidad en el idioma inglés.</li>
											<li>Cubrir los derechos correspondientes.</li>
											<li>Acreditar el actode Recepción Profesional.</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<h2 class="font-regular text-center">Generación 2009-2010 (en competencias)</h2>
						<div class="row">
							<div class="col">
								<div class="card" style="border: none; box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2)">
									<h5 class="card-header font-bold" style="border:none; background-color: #fff">Opciones de Titulación</h5>
									<div class="card-body" style="background-color: #F9FBFD; border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);">
										<h4 class="font-bold">Titulación Integral</h4>
										<p>
											Es la validación de conocimientos, habilidades y actitudes (competencias) que el estudiante adquirió y desarrolló durante su formación profesional.
										</p>
										<p>La titulación integral tiene 2 modalidades:</p>
										<ol>
											<li>
												<strong>Por proyecto</strong>
												<p>Esta modalidad se puede llevar a cabo mediante cualquiera de los siguientes proyectos:</p>
												<ul>
													<li>Residencia Profesional</li>
													<li>De innovación tecnológica</li>
													<li>De investigación</li>
													<li>Estancias,Veranos Científicos</li>
													<li>Tesis</li>
													<li>Otros(aconsiderar, propuesto por las academias)</li>
												</ul>
											</li>
											<li>
												<p></p>
												<strong>EGEL de CENEVAL</strong>
												<p>Obtener resultado de desempeño satisfactorio o sobresaliente en el examen.
													(Esta opción sólo aplica para las carreras que CENEVAL ofrezca)</p>
											</li>
										</ol>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="card" style="border: none; box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2)">
									<h5 class="card-header font-bold" style="border:none; background-color: #fff">Requisitos para obtener el Título Profesional</h5>
									<div class="card-body" style="background-color: #F9FBFD; border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);">
										<ul>
											<li>Aprobar todas las asignaturas.</li>
											<li>Haber realizado el Servicio Social.</li>
											<li>Concluir la Residencia Profesional.</li>
											<li>Solicitar la aprobación de la opción elegida.</li>
											<li>No tener adeudo económico en el Instituto Tecnológico del cual egresó.</li>
											<li>La acreditación de un programa de lengua extranjera,
												presentando un certificado o constancia emitido por una Institución u Organismo Nacional o Internacional con validez oficial.</li>
											<li>Cubrir los derechos correspondientes.</li>
											<li>Acreditar el actode Recepción Profesional.</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col">
						<h2 class="font-regular text-center">Generación 1975-2003</h2>
						<div class="row">
							<div class="col">
								<div class="card" style="border: none; box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2)">
									<h5 class="card-header font-bold" style="border:none; background-color: #fff">Opciones de Titulación</h5>
									<div class="card-body" style="background-color: #F9FBFD; border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);">
										<ul>
											<li>TESIS PROFESIONAL.</li>
											<li>LIBRO DE TEXTO O PROTOTIPOS DIDÁCTICOS.</li>
											<li>PROYECTOS DE INVESTIGACIÓN.</li>
											<li>DISEÑO O REDISEÑO DE EQUIPO, APARATO O MAQUINARIA.</li>
											<li>CURSOS ESPECIALES DE TITULACIÓN.</li>
											<li>EXAMEN GLOBAL POR ÁREAS DE CONOCIMIENTO. (Aplica el EGEL de CENEVAL)</li>
											<li>MEMORIA DE EXPERIENCIA PROFESIONAL.</li>
											<li>ESCOLARIDAD POR PROMEDIO.</li>
											<li>ESCOLARIDAD POR ESTUDIOS DE POSGRADO. Ÿ MEMORIA DE RESIDENCIA PROFESIONAL.</li>
										</ul>
									</div>
								</div>
							</div>
							<div class="col">
								<div class="card" style="border: none; box-shadow: 0 2px 6px 0 hsla(0, 0%, 0%, 0.2)">
									<h5 class="card-header font-bold" style="border:none; background-color: #fff">Requisitos para obtener el Título Profesional</h5>
									<div class="card-body" style="background-color: #F9FBFD; border-radius: 0 0 calc(.25rem - 1px) calc(.25rem - 1px);">
										<ul>
											<li>Aprobar todas las asignaturas.</li>
											<li>Haber realizado el Servicio Social.</li>
											<li>Concluir la Residencia Profesional.</li>
											<li>Solicitar la aprobación de la opción elegida.</li>
											<li>No tener adeudo económico en el Instituto Tecnológico del cual egresó.</li>
											<li>Haber demostrado la capacidad de comprender artículos técnico-científicos de su especialidad en el idioma inglés.</li>
											<li>Cubrir los derechos correspondientes.</li>
											<li>Acreditar el actode Recepción Profesional.</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>
</html>
