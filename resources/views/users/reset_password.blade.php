@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-12 col-md-6 offset-md-3">
				@include('flash::message')
			</div>
		</div>
		<div class="row mb-3">
			<div class="col-12 col-md-6 offset-md-3">
				<div class="card">
					<div class="card-body">
						<form action="{{ route('users.reset.password') }}" method="post">
							@csrf
							<div class="form-group">
								<label for="password">Nueva contraseña</label>
								<div class="input-group">
									<input type="password" class="form-control" name="password" id="password" required minlength="6" maxlength="10">
									<div class="input-group-append">
										<button type="button" class="btn btn-primary" id="btnTooglePassword">
											<i class="fa fa-eye" aria-hidden="true" id="btnTooglePasswordIco"></i>
										</button>
									</div>
								</div>
							</div>
							<div class="form-group">
								<button class="btn btn-success">Actualizar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection

@section('after-extra-scripts')
	<script type="text/javascript">
      // Listen for click events
      document.addEventListener("click", function (event) {
        // If the clicked element isn't our show password checkbox, bail
        if (event.target.id !== "btnTooglePassword" && event.target.id !== "btnTooglePasswordIco") return;

        // Get the password field
        let password = document.querySelector("#password");
        if (!password) return;

        // Check if the password should be shown or hidden
        let ico = document.querySelector("#btnTooglePasswordIco")
        if (password.type === "password") {
          // Show the password
          password.type = "text";
          ico.classList.remove("fa-eye")
          ico.classList.add("fa-eye-slash")
        } else {
          // Hide the password
          ico.classList.remove("fa-eye-slash")
          ico.classList.add("fa-eye")
          password.type = "password";
        }

      }, false);

	</script>
@endsection
