@extends('errors::minimal')

@section('title', __('Server Error'))
@section('code', '422')
@section('message', __($exception->getMessage() ?: 'Forbidden'))
