<ul class="navbar-nav ml-auto">
	<!-- Authentication Links -->
	@guest
		<li class="nav-item">
			<a
				class="nav-link"
				href="{{ route('login') }}"
			>{{ __('Login') }}</a>
		</li>
	@else
		@if (Auth::user()->hasRole(['division', 'administrativo']))
			<li class="nav-item">
				<a
					class="nav-link"
					href="/constancias"
				>Constancias</a>
			</li>
		@endif
		@if(Auth::user()->hasRole(['division', 'servicios_escolares', 'titulacion']))
			<li class="nav-item">
				<a
					href="/servicios-escolares"
					class="nav-link"
				>Estadísticas</a>
			</li>
		@endif
		@if(Auth::user()->hasRole(['servicios_escolares']))
			<li class="nav-item">
				<a
					href="/servicios-escolares/no-adeudos"
					class="nav-link"
				>No adeudos</a>
			</li>
		@endif
		@if(Auth::user()->hasRole(['jefe_depto']))
			<li class="nav-item">
				<a
					href="/titulaciones"
					class="nav-link"
				>Nombramientos</a>
			</li>
		@endif
		@if (Auth::user()->hasRole(['division', 'titulacion']))
			<li class="nav-item">
				<a
					class="nav-link"
					href="/titulaciones"
				>Titulaciones</a>
			</li>
		@endif

		@if (Auth::user()->hasRole(['division']))
			<li class="nav-item dropdown">
				<a
					id="navbarDropdown"
					class="nav-link dropdown-toggle"
					href="#"
					role="button"
					data-toggle="dropdown"
					aria-haspopup="true"
					aria-expanded="false"
					v-pre
				>
					Convalidaciones <span class="caret"></span>
				</a>

				<div
					class="dropdown-menu"
					aria-labelledby="navbarDropdown"
				>
					<a
						class="dropdown-item"
						href="/convalidaciones/solicitudes"
					>Solicitudes</a>
					<a
						class="dropdown-item"
						href="/convalidaciones/carreras"
					>Carreras</a>
					<a
						class="dropdown-item"
						href="/instituciones"
					>Instituciones</a>
				</div>
			</li>
		@endif

		@if (Auth::user()->hasRole(['division', 'titulacion', 'jefe_depto']))
			<li class="nav-item dropdown">
				<a
					id="navbarDropdown"
					class="nav-link dropdown-toggle"
					role="button"
					data-toggle="dropdown"
					aria-haspopup="true"
					aria-expanded="false"
					v-pre
				>
					Catálogos <span class="caret"></span>
				</a>
				<div
					class="dropdown-menu"
					aria-labelledby="navbarDropdown"
				>
					@if (Auth::user()->hasRole(['division', 'titulacion']))
						<a
							class="dropdown-item"
							href="{{ route('oficios.index') }}"
						>Números de memo</a>
					@endif
					@if(Auth::user()->hasRole(['division']))
						<a
							class="dropdown-item"
							href="{{ route('departamentos.index') }}"
						>Departamentos</a>
					@endif
					@if(Auth::user()->hasRole(['division', 'jefe_depto']))
						<a
							class="dropdown-item"
							href="{{ route('maestros.index') }}"
						>Maestros</a>
					@endif
					@if(Auth::user()->hasRole(['division']))
						<a
							class="dropdown-item"
							href="/alumnos"
						>Alumnos</a>
						<a
							class="dropdown-item"
							href="/administracion/usuarios"
						>Usuarios</a>
						<a
							class="dropdown-item"
							href="{{ route('carreas.index') }}"
						>Carreras</a>
					@endif
				</div>
			</li>
		@endif

		@if (Auth::user()->hasRole(['division', 'financieros', 'biblioteca', 'extraescolares', 'depto_academico', 'gestion_vinculacion', 'jefe_depto']))
			<li class="nav-item dropdown">
				<a
					id="navbarDropdown"
					class="nav-link dropdown-toggle"
					role="button"
					data-toggle="dropdown"
					aria-haspopup="true"
					aria-expanded="false"
					v-pre
				>
					Solicitudes <span class="caret"></span>
				</a>

				<div
					class="dropdown-menu"
					aria-labelledby="navbarDropdown"
				>
					<a
						class="dropdown-item"
						href="{{ route('solicitudes.no.adeudos') }}"
					>No adeudos</a>
				</div>
			</li>
		@endif

		<li class="nav-item dropdown">
			<a
				id="navbarDropdown"
				class="nav-link dropdown-toggle"
				href="#"
				role="button"
				data-toggle="dropdown"
				aria-haspopup="true"
				aria-expanded="false"
				v-pre
			>
				{{ Auth::user()->name }} <span class="caret"></span>
			</a>

			<div
				class="dropdown-menu"
				aria-labelledby="navbarDropdown"
			>
				<a
					class="dropdown-item"
					href="{{ route('users.reset.password') }}"
				>
					Cambiar contraseña
				</a>
				<a
					class="dropdown-item"
					href="{{ route('logout') }}"
					onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();"
				>
					{{ __('Salir') }}
				</a>
				<form
					id="logout-form"
					action="{{ route('logout') }}"
					method="POST"
					style="display: none;"
				>
					@csrf
				</form>
			</div>
		</li>
	@endguest
</ul>
