@if(Auth::user()->hasRole(['jefe_depto']))
	<li class="nav-item">
		<a href="/titulaciones" class="nav-link">Nombramientos</a>
	</li>
@endif

@if(Auth::user()->hasRole(['titulacion']))
	<li class="nav-item">
		<a href="/titulaciones" class="nav-link">Solicitudes</a>
	</li>
@endif

@if (Auth::user()->hasRole(['division', 'titulacion']))
	<li class="nav-item">
		<a href="{{ route('oficios.index') }}" class="nav-link">Números de memo</a>
	</li>
@endif

@if(Auth::user()->hasRole(['division']))
	<li class="nav-item">
		<a href="{{ route('departamentos.index') }}" class="nav-link">Departamentos</a>
	</li>
@endif

@if(Auth::user()->hasRole(['division', 'jefe_depto']))
	<li class="nav-item">
		<a href="{{ route('maestros.index') }}" class="nav-link">Maestros</a>
	</li>
@endif

@if(Auth::user()->hasRole(['division']))
	<li class="nav-item">
		<a href="{{ route('carreas.index') }}" class="nav-link">Carreras</a>
	</li>
@endif

{{--@if(Auth::user()->hasRole(['division']))
	<li class="nav-item">
		<a href="{{ route('alumnos.index') }}" class="nav-link">Alumnos</a>
	</li>
@endif--}}

@if(Auth::user()->hasRole(['division', 'servicios_escolares', 'titulacion']))
	<li class="nav-item">
		<a href="/servicios-escolares" class="nav-link">Estadísticas</a>
	</li>
@endif
