@extends('layouts.base')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-header">
						Maestros
					</div>
					<div class="card-body p-0">
						<section class="new-departamento pt-3 pr-3 pl-3" style="background-color: #FAFAFA">
							{!! Form::open(['route'=>['maestros.store'], 'class'=>'form-horizontal', 'method'=>'post']) !!}
							<div class="row">
								<div class="col form-group">
									<div class="input-group">
										<div class="input-group-prepend">
                                            <span class="input-group-text" style="background: #fff">
                                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                            </span>
										</div>
										{!! Form::text('nombre_completo', NULL, ['class'=>'form-control', 'required', 'placeholder'=>'Nombre completo']) !!}
									</div>
								</div>
								<div class="col form-group">
									<div class="input-group">
										<div class="input-group-prepend">
                                            <span class="input-group-text" style="background: #fff">
                                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                            </span>
										</div>
										{!! Form::text('especialidad', NULL, ['class'=>'form-control', 'required', 'placeholder'=>'Especialidad']) !!}
									</div>
								</div>
								<div class="col form-group">
									<div class="input-group">
										<div class="input-group-prepend">
                                            <span class="input-group-text" style="background: #fff">
                                                <i class="fa fa-slack" aria-hidden="true"></i>
                                            </span>
										</div>
										{!! Form::text('cedula_profesional', NULL, ['class'=>'form-control', 'required', 'placeholder'=>'Cédula profesional' ]) !!}
									</div>
								</div>
								<div class="col-md-2 form-group">
									<button class="btn btn-primary btn-block">Guardar</button>
								</div>
							</div>
							{!! Form::close() !!}
						</section>
					</div>
					<div class="card-body table-responsive p-0">
						<table class="table">
							<thead>
								<tr>
									<th width="1%">#</th>
									<th width="20%">Nombre</th>
									<th width="20%">Especialidad</th>
									<th width="10%">Cédula profesional</th>
									<th width="1%">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@forelse($maestros as $maestro)
									{!! Form::model($maestro, ['route'=>['maestros.update', $maestro->id], 'class'=>'', 'method'=>'patch']) !!}
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>
											<div class="input-group">
												<div class="input-group-prepend">
                                            <span class="input-group-text" style="background: #fff">
                                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                            </span>
												</div>
												{!! Form::text('nombre_completo', NULL, ['class'=>'form-control', 'required']) !!}
											</div>
										</td>
										<td>
											<div class="input-group">
												<div class="input-group-prepend">
                                            <span class="input-group-text" style="background: #fff">
                                                <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                                            </span>
												</div>
												{!! Form::text('especialidad', NULL, ['class'=>'form-control', 'required']) !!}
											</div>
										</td>
										<td>
											<div class="input-group">
												<div class="input-group-prepend">
                                            <span class="input-group-text" style="background: #fff">
                                                <i class="fa fa-slack" aria-hidden="true"></i>
                                            </span>
												</div>
												{!! Form::text('cedula_profesional', NULL, ['class'=>'form-control', 'required']) !!}
											</div>
										</td>
										<td>
											<button class="btn btn-primary">Actualizar</button>
										</td>
									</tr>
									{!! Form::close() !!}
								@empty
									<tr>
										<td colspan="4"><h4>Sin resultados</h4></td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('extra-scripts')
@stop