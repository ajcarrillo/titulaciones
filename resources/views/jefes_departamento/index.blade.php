@extends('layouts.base')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-header">
						Departamentos
					</div>
					<div class="card-body p-0">
						<section class="new-departamento pt-3 pr-3 pl-3" style="background-color: #FAFAFA">
							{!! Form::open(['route'=>['departamentos.store'], 'class'=>'form-horizontal', 'method'=>'post']) !!}
							<div class="form-row">
								<div class="col-md-3 form-group">
									<div class="input-group">
										<div class="input-group-prepend">
                                            <span class="input-group-text" style="background: #fff">
                                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                            </span>
										</div>
										{!! Form::select('responsable_id', $usuarios, NULL, ['class'=>'custom-select', 'required','placeholder'=>'Seleccione...']) !!}
									</div>
								</div>
								<div class="col form-group">
									<div class="input-group">
										<div class="input-group-prepend">
                                                <span class="input-group-text" style="background: #fff">
                                                    <i class="fa fa-building-o" aria-hidden="true"></i>
                                                </span>
										</div>
										<input type="text" name="jefatura" class="form-control" placeholder="Departamento">
									</div>
								</div>
								<div class="col-md-2 form-group mb-0">
									<button class="btn btn-primary btn-block">Guardar</button>
								</div>
							</div>
							{!! Form::close() !!}
						</section>
					</div>
					<div class="card-body p-0 table-responsive">
						<table class="table">
							<thead>
								<tr>
									<th width="1%">#</th>
									<th width="40%">Responsable</th>
									<th>Departamento</th>
									<th width="5%">Opciones</th>
								</tr>
							</thead>
							<tbody>
								@forelse($departamentos as $departamento)
									{!! Form::model($departamento, ['route'=>['departamentos.upddate', $departamento->id], 'method'=>'patch']) !!}
									<tr>
										<td>{{ $loop->iteration }}</td>
										<td>
											<div class="input-group">
												<div class="input-group-prepend">
                                            <span class="input-group-text" style="background: #fff">
                                                <i class="fa fa-user-o" aria-hidden="true"></i>
                                            </span>
												</div>
												{!! Form::select('responsable_id', $usuarios, NULL, ['class'=>'custom-select', 'required','placeholder'=>'Seleccione...']) !!}
											</div>
										</td>
										<td>
											<div class="input-group">
												<div class="input-group-prepend">
                                                <span class="input-group-text" style="background: #fff">
                                                    <i class="fa fa-building-o" aria-hidden="true"></i>
                                                </span>
												</div>
												{!! Form::text('jefatura', NULL, ['class'=>'form-control', 'required']) !!}
											</div>
										</td>
										<td>
											<button class="btn btn-primary">Actualizar</button>
										</td>
									</tr>
									{!! Form::close() !!}
								@empty
									<tr>
										<td colspan="3"><h4>Sin resultados</h4></td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('extra-scripts')
@stop