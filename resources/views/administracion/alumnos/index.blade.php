@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col">@include('flash::message')</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-header">
						Alumnos
					</div>
					<div class="card-body pb-0">
						<form action="{{ route('alumnos.importar') }}" method="post" enctype="multipart/form-data">
							@csrf
							<div class="form-group">
								<label for="">Subir archivo</label>
								<input type="file"
								       accept=".xls"
								       class="form-control-file"
								       name="import_file"
								       id=""
								       placeholder=""
								       aria-describedby="fileHelpId">
								<small id="fileHelpId" class="form-text text-muted">Archivo en formato xls</small>
							</div>
							<div class="form-group mb-0">
								<button class="btn btn-success">Subir</button>
							</div>
						</form>
					</div>
					@if(Session::has('file_name'))
						<div class="card-body pb-0">
							<div class="alert alert-success" role="alert">
								<p class="mb-0 d-inline-block">Ejecute en consola lo siguiente:</p>
								<div class="input-group">
									<input id="copy-command" type="text" class="form-control" value="{{ Session::get('file_name') }}" readonly>
									<div class="input-group-append">
										<button class="btn btn-success btn-copy" type="button" data-clipboard-target="#copy-command">
											<i class="fa fa-clipboard" aria-hidden="true"></i>
										</button>
									</div>
								</div>
							</div>
						</div>
					@endif
					<div class="card-body">
						<form action="{{ route('alumnos.index') }}" method="get">
							<label for="">Buscar</label>
							<div class="form-group mb-0">
								<input type="text"
								       class="form-control"
								       name="search"
								       placeholder="Ingresa número de control, nombre o carrera"
								       value="{{ request('search') }}"
								       autofocus>
							</div>
						</form>
					</div>
					<div style="align-self: center;">
						{{ $alumnos->links() }}
					</div>
					<div class="card-body p-0">
						<table class="table">
							<thead>
								<tr>
									<th>Num Control</th>
									<th>Nombre</th>
									<th>Sexo</th>
									<th>Carrera</th>
								</tr>
							</thead>
							<tbody>
								@forelse($alumnos as $alumno)
									<tr>
										<td scope="row">{{ $alumno->numero_control }}</td>
										<td>{{ $alumno->nombre_apellido }}</td>
										<td>{{ $alumno->sexo }}</td>
										<td>{{ "{$alumno->carrera->descripcion} / {$alumno->carrera->clave_oficial}" }}</td>
									</tr>
								@empty
									<tr>
										<td colspan="4"><p class="text-center"><b>Sin alumnos</b></p></td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
					<div style="align-self: center;">
						{{ $alumnos->links() }}
					</div>
				</div>
			</div>
		</div>
	</div>
@stop

@section('after-extra-scripts')
	<script type="text/javascript">
      new ClipboardJS(".btn-copy");
	</script>
@endsection
