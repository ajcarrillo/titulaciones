@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col">
				@include('flash::message')
			</div>
		</div>
		<div class="row mb-3">
			<div class="col">
				<div class="card">
					<div class="card-body">
						<form action="{{ route('carreas.importar') }}" method="post" enctype="multipart/form-data">
							@csrf
							<div class="form-group">
								<label for="">Importar carreras</label>
								<input
									accept=".xls"
									type="file"
									class="form-control-file"
									name="import_file"
									id=""
									placeholder=""
									aria-describedby="fileHelpId">
								<small id="fileHelpId" class="form-text text-muted">Archivo en formato cvs</small>
							</div>
							<div class="form-group">
								<button class="btn btn-success">Subir archivo</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card rounded-0">
					<div class="card-body">
						<form action="" method="get">
							<div class="form-group">
								<label for="">Buscar</label>
								<input type="text"
								       class="form-control"
								       name="search"
								       placeholder="Introduce nombre de la carrera"
								       value="{{ request('search') }}"
								       autofocus>
							</div>
						</form>
					</div>
				</div>
				@foreach($carreras as $carrera)
					<div class="card rounded-0">
						<div class="card-body">
							<div class="row">
								<div class="col"><b>{{ $carrera->descripcion }}</b></div>
							</div>
							<div class="row mb-3">
								<div class="col">{{ optional($carrera->jefeDepartamento)->jefatura }}</div>
								<div class="col">{{ optional(optional($carrera->jefeDepartamento)->responsable)->name }}</div>
							</div>
							<form action="{{ route('carreas.departamento.update') }}" method="post">
								@csrf
								@method('PATCH')
								<input type="hidden" name="sii_id" value="{{ $carrera->sii_id }}">
								<div class="row">
									@foreach($departamentos as $departamento)
										<div class="col">
											<label class="checkbox-inline">
												<input
													type="radio"
													name="jefe_departamento_id"
													id="departamento-{{ $departamento->id }}"
													value="{{ $departamento->id }}"
													onclick="let form = this.closest('form'); form.submit();"
													{{ $departamento->id == $carrera->jefe_departamento_id ? 'checked' : '' }}>
												{{ $departamento->jefatura }}
											</label>
										</div>
									@endforeach
								</div>
							</form>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@stop
