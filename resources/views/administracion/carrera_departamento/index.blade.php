@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col">
				@include('flash::message')
			</div>
		</div>
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-header">
						Carreras
					</div>
					<div class="card-body">
						<form action="{{ route('departamento.carreras.store') }}" method="post">
							@csrf
							<div class="form-group">
								<label class="font-weight-bold">Departamentos</label>
								@foreach($departamentos as $departamento)
									<div class="form-check mb-3">
										<label class="form-check-label">
											<input
												type="radio"
												class="form-check-input"
												name="jefe_departamento_id"
												id=""
												value="{{ $departamento->id }}">
											{{ $departamento->jefatura }}
										</label>
									</div>
								@endforeach
							</div>
							<div class="form-group">
								<label for="carrera_id" class="font-weight-bold">Carreras</label>
								<select
									name="carrera_id[]"
									id="carrera_id"
									class="form-control"
									size="10"
									multiple>
									@foreach($carreras as $carrera)
										<option value="{{ $carrera->id }}">{{ $carrera->descripcion }}</option>
									@endforeach
								</select>
							</div>
							<div class="form-group mb-0">
								<button class="btn btn-success">Guardar</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@stop
