@extends('layouts.base')

@section('extra-head')
@stop

@section('content')
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="shadow-sm p-3 mb-3 bg-white rounded">
					{!! Form::open(['method'=>'get']) !!}
					<div class="row">
						<div class="col">
							{!! Form::label('year', 'Buscar por año:', ['class'=>'control-label']) !!}
							{!! Form::number('year', NULL, ['class'=>'form-control text-right']) !!}
						</div>
						<div class="col">
							{!! Form::label('numero_oficio', 'Buscar por número de oficio:', ['class'=>'control-label']) !!}
							{!! Form::number('numero_oficio', NULL, ['class'=>'form-control text-right']) !!}
						</div>
						<input type="submit" hidden/>
					</div>
					{!! Form::close() !!}
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-header">
						Generar números de oficio
					</div>
					<div class="card-body">
						{!! Form::open(['route'=>['oficios.store'], 'method'=>'post']) !!}
						<div class="row">
							<div class="col">
								{!! Form::label('de', 'De:', ['class'=>'control-label']) !!}
								{!! Form::number('de', NULL, ['class'=>'form-control', 'required', 'min'=>1, 'step'=>1]) !!}
							</div>
							<div class="col">
								{!! Form::label('hasta', 'Hasta:', ['class'=>'control-label']) !!}
								{!! Form::number('hasta', NULL, ['class'=>'form-control', 'required', 'min'=>1, 'step'=>1]) !!}
							</div>
							<div class="col">
								{!! Form::label('new_year', 'Año:', ['class'=>'control-label']) !!}
								{!! Form::number('new_year', NULL, ['class'=>'form-control', 'required', 'min'=>1, 'step'=>1]) !!}
							</div>
							<input type="submit" hidden/>
						</div>
						{!! Form::close() !!}
						<p class="form-text text-muted">
							* Al terminar de ingresar la información presione enter
						</p>
					</div>
				</div>
			</div>
		</div>
		<p></p>
		<div class="row">
			<div class="col">
				<div class="card">
					<div class="card-header">
						Números de oficio del año <strong>{{ $year }}</strong>
					</div>
					<div class="card-body table-responsive p-0">
						<table class="table">
							<thead>
								<tr>
									<th width="5%">Oficio</th>
									<th width="5%">Año</th>
									<th width="5%">Estatus</th>
									<th width="">Trámite de:</th>
								</tr>
							</thead>
							<tbody>
								@forelse($oficios as $oficio)
									<tr>
										<td>{{ $oficio->numero_oficio }}</td>
										<td>{{ $oficio->year }}</td>
										<td>
											@if($oficio->activo)
												<span class="badge badge-info">Disponible</span>
											@else
												<span class="badge badge-primary">No disponible</span>
											@endif
										</td>
										<td>{{ $oficio->nombramiento->alumno->nombre_apellido ?? '' }}</td>
									</tr>
								@empty
									<tr>
										<td class="4"><h4>Sin resultados</h4></td>
									</tr>
								@endforelse
							</tbody>
						</table>
					</div>
				</div>
				<p></p>
				{{ $oficios->links() }}
			</div>
		</div>
	</div>
@stop

@section('extra-scripts')
@stop
