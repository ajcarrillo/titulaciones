@extends('layouts.base')

@section('content')
	<div id="app" class="pt-4">
		<servicios></servicios>
	</div>
@endsection

@section('extra-scripts')
	<script src="{{ mix('js/servicios_escolares.js') }}"></script>
@endsection
