@extends('layouts.app')

@section('content')
	<router-view></router-view>
@endsection

@section('extra-scripts')
	<script type="text/javascript">
      window.Maestros = {!! $maestros !!}
	</script>
@endsection
