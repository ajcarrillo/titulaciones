<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Titulaciones\Http\Controllers\ActasTitulacionController;
use Titulaciones\Http\Controllers\AlumnoController;
use Titulaciones\Http\Controllers\Catalogos\OpcionController;
use Titulaciones\Http\Controllers\Catalogos\TipoConstanciaController;
use Titulaciones\Http\Controllers\ConstanciaController;
use Titulaciones\Http\Controllers\CustomConstanciasController;
use Titulaciones\Http\Controllers\DownloadExcelAnexoTresComputoController;
use Titulaciones\Http\Controllers\GenerarAnexoTresController;
use Titulaciones\Http\Controllers\HomeController;
use Titulaciones\Http\Controllers\InstitucionController;
use Titulaciones\Http\Controllers\ResetPasswordController;
use Titulaciones\Http\Controllers\ServiciosEscolares\NoAudeudosController;
use Titulaciones\Http\Controllers\ServiciosEscolares\UpdateNoAdeudoController;
use Titulaciones\Http\Controllers\ServiciosEscolares\UpdateObservacionController;
use Titulaciones\Http\Controllers\SolicitudNoAdeudoController;
use Titulaciones\Http\Controllers\TitulacionController;
use Titulaciones\Http\Controllers\TitulacionSinodalController;

Route::get('/', function () {
    return redirect()->route('login');
    //return view('welcome');
});


Route::get('/create-word', 'WordTestController@test')->name('create.word');
Route::get('/test-replace-word', 'WordTestController@testReplace')->name('replace.word');
Route::get('/generar-memo-sinodales/{nombramiento}', 'MemoSinodalesController@generarMemoSinodales')->name('nombramiento.memo.sinodales');
Route::get('/generar-nombramiento/{nombramiento}', 'NombramientoController@generarNombramiento')->name('nombramiento.nombramiento');
Route::get('/generar-juramento/{nombramiento}', 'JuramentoController@generarJuramento')->name('nombramiento.juramento');
Route::get('/generar-acta/{nombramiento}', 'ActaExamenController@generarActaExamen')->name('nombramiento.acta');
Route::get('/generar-intervencion-general/{nombramiento}', 'SolicitudIntervencionGeneralController')->name('nombramiento.intervencion.general');
Route::get('/generar-intervencion-sustentante/{nombramiento}', 'SolicitudIntervencionSustentanteController')->name('nombramiento.intervencion.sustentante');

Auth::routes($options = [ 'register' => false, 'reset' => false ]);

Route::group([ 'middleware' => [ 'auth' ], 'prefix' => '/home' ], function () {
    Route::any('/{all?}', [ HomeController::class, 'index' ])->where([ 'all' => '.*' ]);
});

Route::group([ 'middleware' => [ 'auth' ], 'prefix' => '/administracion' ], function () {
    Route::any('/{all?}', [ HomeController::class, 'index' ])->where([ 'all' => '.*' ]);
});

Route::prefix('/constancias')
    ->name('constancias.')
    ->middleware([ 'auth', 'role:division,administrativo' ])
    ->group(function () {

        Route::get('/', [ ConstanciaController::class, 'index' ])->name('index');
        Route::get('/custom/download/{id}', [ CustomConstanciasController::class, 'download' ])->name('custom.download');
        Route::get('/download/{id}', [ ConstanciaController::class, 'download' ])->name('download');
        Route::get('/nueva', [ ConstanciaController::class, 'create' ])->name('create');
        Route::post('/', [ ConstanciaController::class, 'store' ])->name('store');


        Route::prefix('/catalogos')
            ->name('catalogos.')
            ->group(function () {
                Route::prefix('/tipo-constancias')
                    ->name('tipos.contancias.')
                    ->group(function () {
                        Route::get('/', [ TipoConstanciaController::class, 'index' ])->name('index');
                        Route::post('/', [ TipoConstanciaController::class, 'store' ])->name('store');
                    });
            });
    });

Route::prefix('/usuarios')
    ->middleware([ 'auth' ])
    ->name('users.')
    ->group(function () {
        Route::get('/reset-password', [ ResetPasswordController::class, 'edit' ])->name('reset.password');
        Route::post('/reset-password', [ ResetPasswordController::class, 'update' ])->name('reset.password');
    });

Route::prefix('/titulaciones')
    ->name('titulaciones.')
    ->middleware([ 'auth', 'role:titulacion,division,jefe_depto' ])
    ->group(function () {

        Route::get('/', [ TitulacionController::class, 'index' ])->name('index');
        Route::post('/', [ TitulacionController::class, 'store' ])->name('store');
        Route::get('/nueva', [ TitulacionController::class, 'create' ])->name('create');
        Route::patch('/{titulacion}', [ TitulacionController::class, 'update' ])->name('update');
        Route::get('/{titulacion}/editar', [ TitulacionController::class, 'edit' ])->name('edit');
        Route::get('/{nombramiento}/generar-anexo-tres', [ GenerarAnexoTresController::class, 'download' ])->name('generar.anexo');
        Route::post('/{nombramiento}/representante', [ GenerarAnexoTresController::class, 'update' ])->name('update.representante');

        Route::prefix('/{titulacion}/sinodales')
            ->name('sinodales.')
            ->group(function () {
                Route::post('/', [ TitulacionSinodalController::class, 'store' ])->name('store');
                Route::get('/asignar', [ TitulacionSinodalController::class, 'create' ])->name('create');
                Route::get('/editar', [ TitulacionSinodalController::class, 'edit' ])->name('edit');
            });


        Route::prefix('catalogos')
            ->name('catalogos.')
            ->middleware([ 'auth' ])
            ->group(function () {
                Route::prefix('opciones-titulacion')
                    ->name('opciones.titulacion.')
                    ->group(function () {
                        Route::get('/', [ OpcionController::class, 'index' ])->name('index');
                    });
            });
    });

Route::resource('departamentos', 'JefeDepartamentoController', [
    'parameters' => [ 'departamentos' => 'departamento' ],
    'only'       => [ 'index', 'store', 'update' ],
    'names'      => [
        'index'  => 'departamentos.index',
        'store'  => 'departamentos.store',
        'update' => 'departamentos.upddate',
    ],
])->middleware([ 'auth', 'role:division' ]);

Route::prefix('/departamentos/carreras')
    ->name('departamento.carreras.')
    ->middleware([ 'auth', 'role:titulacion' ])
    ->group(function () {
        Route::get('/', 'CarreraDepartamentoController@index')->name('index');
        Route::post('/', 'CarreraDepartamentoController@store')->name('store');
    });

Route::resource('maestros', 'MaestroController', [
    'parameters' => [ 'maestros' => 'maestro' ],
    'only'       => [ 'index', 'create', 'store', 'edit', 'update' ],
    'names'      => [
        'index'  => 'maestros.index',
        'store'  => 'maestros.store',
        'update' => 'maestros.update',
    ],
])->middleware([ 'auth', 'role:division,jefe_depto' ]);

Route::group([ 'prefix' => '/numeros-de-oficio', 'middleware' => [ 'auth', 'role:division,titulacion' ] ], function () {
    Route::get('/', 'NumeroDeOficioController@index')->name('oficios.index');
    Route::post('/', 'NumeroDeOficioController@store')->name('oficios.store');
});

Route::group([ 'prefix' => '/reportes', 'middleware' => [ 'auth', 'role:division,servicios_escolares' ] ], function () {
    Route::get('/nombramientos', 'ReporteTitulacionesController@download')
        ->name('reportes.nombramientos');
    Route::get('/actas-titulacion', [ ActasTitulacionController::class, 'download' ])
        ->name('reportes.actas.titulacion');
    Route::get('/anexo-tres-computo', [ DownloadExcelAnexoTresComputoController::class, 'download' ])
        ->name('reportes.anexo.tres.computo');
});

Route::prefix('/servicios-escolares')
    ->name('servicios.escolares.')
    ->middleware([ 'auth', 'role:division,servicios_escolares,titulacion' ])
    ->group(function () {
        Route::get('/', 'HomeController@serviciosEscolares');
        Route::get('/no-adeudos', [ NoAudeudosController::class, 'index' ])->name('no.adeudos.index');
        Route::post('/no-adeudos', [ NoAudeudosController::class, 'store' ])->name('no.adeudos.store');
    });

Route::prefix('/carreras')
    ->name('carreas.')
    ->middleware([ 'auth', 'role:titulacion' ])
    ->group(function () {
        Route::patch('/actualiza-departamento', 'ActualizarCarreraDepartamentoController@update')->name('departamento.update');
        Route::post('/importar', 'ImportarCarrerasController@import')->name('importar');
        Route::get('/', 'CarrerasController@index')->name('index');
    });

Route::prefix('/alumnos')
    ->name('alumnos.')
    ->middleware([ 'auth', 'role:titulacion' ])
    ->group(function () {
        Route::post('/importar', 'ImportarAlumnosController@import')->name('importar');
        Route::get('/', 'AlumnoController@index')->name('index');
        Route::get('/nuevo', [ AlumnoController::class, 'create' ])->name('create');
        Route::post('/', [ AlumnoController::class, 'store' ])->name('store');
    });

Route::prefix('instituciones')
    ->name('instituciones.')
    ->middleware([ 'auth', 'role:division' ])
    ->group(function () {
        Route::get('/', [ InstitucionController::class, 'index' ])->name('index');
        Route::post('/', [ InstitucionController::class, 'store' ])->name('store');
        Route::get('/crear', [ InstitucionController::class, 'create' ])->name('create');
        Route::patch('{institucion}', [ InstitucionController::class, 'update' ])->name('update');
        Route::get('{institucion}/editar', [ InstitucionController::class, 'edit' ])->name('edit');
    });

Route::prefix('solicitudes')
    ->middleware([ 'auth' ])
    ->name('solicitudes.')
    ->group(function () {
        Route::get('no-adeudos', [ SolicitudNoAdeudoController::class, 'index' ])
            ->middleware([ 'role:financieros,extraescolares,division,depto_academico,gestion_vinculacion,biblioteca,jefe_depto' ])
            ->name('no.adeudos');
        Route::patch('no-adeudos/{adeudo}/update-field', [ UpdateNoAdeudoController::class, 'update' ])
            ->middleware([ 'role:financieros,extraescolares,division,depto_academico,gestion_vinculacion,biblioteca,jefe_depto' ])
            ->name('no.adeudos.update.field');
        Route::patch('no-adeudos/{adeudo}/observaciones', [ UpdateObservacionController::class, 'update' ])
            ->middleware([ 'role:financieros,extraescolares,division,depto_academico,gestion_vinculacion,biblioteca,jefe_depto' ])
            ->name('no.adeudos.update.observaciones');
    });

Route::get('/phpinfo', function () {
    phpinfo();
})->middleware('auth');
