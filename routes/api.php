<?php

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Api\v1\CheckHorarioNombramientoController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/alumnos', 'Api\AlumnoController@index')
    ->middleware('auth:api')
    ->name('alumnos.search');

Route::group([ 'middleware' => [ 'auth:api' ], 'prefix' => '/carreras' ], function () {
    Route::get('/', 'Api\CarreraController@carreras')->name('endpoint.carreras');
});

Route::get('/horarios', 'Api\HorarioController@index')
    ->middleware('auth:api')
    ->name('horarios.index');

Route::group([ 'middleware' => [ 'auth:api' ], 'prefix' => '/opciones' ], function () {
    Route::get('/', 'Api\OpcionController@index')->name('opciones.index');
});

Route::group([ 'middleware' => [ 'auth:api' ], 'prefix' => '/modulos' ], function () {
    Route::get('/', 'Api\ModuloController@index')->name('modulos.index');
});

Route::group([ 'middleware' => [ 'auth:api' ], 'prefix' => '/maestros' ], function () {
    Route::get('/search', 'Api\MaestroController@searchMaestros')->name('endpoint.maestros.search');
});

Route::group([ 'middleware' => [ 'auth:api' ], 'prefix' => '/nombramientos' ], function () {
    Route::post('/{nombramiento}/sinodales', 'Api\NombramientoController@storeSinodales')->name('endpoint.nombramientos.store.sinodales');
    Route::get('/{nombramiento}/sinodales', 'Api\NombramientoController@getSinodales')->name('endpoint.nombramientos.store.sinodales');
    Route::get('/all', 'Api\NombramientoController@index')->name('endpoint.nombramientos');
    Route::post('/', 'Api\NombramientoController@store')->name('endpoint.nombramiento.store');
    Route::get('/', 'Api\NombramientoController@getData')->name('endpoint.nombramiento.getData');
});

Route::get('/titulaciones', 'Api\TitulacionController@index')
    ->middleware('auth:api')
    ->name('estadisticas.titulaciones.index');

Route::resource('usuarios', 'Api\UserController', [ 'only' => [
    'index', 'store', 'show', 'update',
] ]);

Route::resource('roles', 'Api\RolController', [ 'only' => [
    'index',
] ]);

Route::post('/{nombramiento}/numero-oficio', 'Api\UpdateNumeroOficioController@update')
    ->middleware('auth:api')
    ->name('update.numero.oficio');

Route::get('/{nombramiento}/numero-oficio', 'Api\UpdateNumeroOficioController@show')
    ->middleware('auth:api')
    ->name('show.numero.oficio');

Route::patch('/enabled-user/{usuario}', 'Api\EnabledUserController@update')->name('enabled-users');
Route::delete('/enabled-user/{usuario}', 'Api\EnabledUserController@destroy')->name('disabled-users');
Route::patch('/change-password/{usuario}', 'Api\ResetPasswordController@update')->name('change-password');

Route::get('/check-horario/', [ CheckHorarioNombramientoController::class, 'check' ])
    ->middleware('auth:api')
    ->name('check.horario');