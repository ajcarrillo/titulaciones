<?php
//path: api/v1

use Titulaciones\Http\Controllers\Api\v1\AlumnoController;

Route::get('alumnos', [ AlumnoController::class, 'index' ])->name('alumnos.index');
