<?php

namespace Titulaciones\Models;

use Illuminate\Database\Eloquent\Model;
use Titulaciones\Traits\FilterBy;

class Maestro extends Model
{
    use FilterBy;

    protected $table   = 'maestros';
    protected $guarded = [];
}
