<?php

namespace Titulaciones\Models;

use Convalidaciones\Models\Asignatura;
use Illuminate\Database\Eloquent\Model;
use Titulaciones\Traits\FilterBy;

class Carrera extends Model
{
    use FilterBy;

    protected $table    = 'carreras';
    protected $fillable = [
        'sii_id',
        'descripcion',
        'grado',
        'jefe_departamento_id',
        'clave_oficial',
        'estado',
        'tipo',
        'nivel_escolar',
        'reticula',
    ];
    protected $appends  = [ 'clave_nombre', 'descripcion_completa', 'descripcion_estado' ];

    public function getDescripcionEstadoAttribute()
    {
        return "{$this->estado[0]}R-{$this->reticula}";
    }

    public function getDescripcionCompletaAttribute()
    {
        return "{$this->estado[0]}R-{$this->reticula} {$this->clave_oficial}/{$this->descripcion}";
    }

    public function getClaveNombreAttribute()
    {
        return "{$this->clave_oficial}/{$this->descripcion}";
    }

    public function asignaturas()
    {
        return $this->hasMany(Asignatura::class, 'carrera_id');
    }

    public function alumnos()
    {
        return $this->hasMany(Alumno::class, 'carrera_id');
    }

    public function nombramientos()
    {
        return $this->hasMany(Nombramiento::class, 'carrera_id');
    }

    public function jefeDepartamento()
    {
        return $this->belongsTo(JefeDepartamento::class, 'jefe_departamento_id');
    }

    public function modulos()
    {
        return $this->belongsToMany(Modulo::class, 'carrera_modulo', 'carrera_id', 'modulo_id');
    }
}
