<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 26/09/20
 * Time: 1:07
 */

namespace Titulaciones\Models;


use Illuminate\Database\Eloquent\Model;

class TipoConstancia extends Model
{
    protected $table   = 'tipo_constancias';
    protected $guarded = [];
    protected $casts   = [
        'variables' => 'array',
    ];

    public function setDescripcionAttribute($value)
    {
        $this->attributes['descripcion'] = mb_strtoupper($value, 'UTF-8');
    }
}
