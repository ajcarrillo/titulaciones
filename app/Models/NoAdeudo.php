<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/02/21
 * Time: 0:28
 */

namespace Titulaciones\Models;


use Illuminate\Database\Eloquent\Model;
use Titulaciones\Traits\FilterBy;

class NoAdeudo extends Model
{
    use FilterBy;

    protected $table   = 'no_adeudos';
    protected $guarded = [];

    public function alumno()
    {
        return $this->belongsTo(Alumno::class, 'alumno_id');
    }

    public function scopeByRole($query, $column)
    {
        return $query->where($column, NULL)
            ->orWhere($column, 0)
            ->orWhere($column, 1);
        /*switch ($role) {
            case 'biblioteca':
                return $query->where('adeuda_biblioteca', NULL);
            case 'extraescolares':
                return $query->where('adeuda_extraescolares', NULL);
            case 'gestion_vinculacion':
                return $query->where('adeuda_gestionvinculacion', NULL);
            case 'depto_academico':
                return $query->where('adeuda_deptoacademico', NULL);
            case 'financieros':
                return $query->where('adeuda_financieros', NULL);
            case 'division':
                return $query->where('adeuda_divisionestudios', NULL);
        }*/
    }
}
