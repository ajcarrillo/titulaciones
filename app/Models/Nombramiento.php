<?php

namespace Titulaciones\Models;

use Illuminate\Database\Eloquent\Model;
use Titulaciones\Traits\FilterBy;

class Nombramiento extends Model
{
    use FilterBy;

    protected $table   = 'nombramientos';
    protected $guarded = [];
    protected $dates   = [
        'fecha' => 'Y-m-d',
    ];

    public function memo()
    {
        return $this->belongsTo(NumeroOficio::class, 'numero_memo_id');
    }

    public function alumno()
    {
        return $this->belongsTo(Alumno::class, 'alumno_id');
    }

    public function opcion()
    {
        return $this->belongsTo(Opcion::class, 'opcion_id');
    }

    public function modulo()
    {
        return $this->belongsTo(Modulo::class, 'modulo_id')->withDefault([ 'empty' => true ]);
    }

    public function horario()
    {
        return $this->belongsTo(Horario::class, 'horario_id');
    }

    public function sinodales()
    {
        return $this->hasMany(Sinodal::class, 'nombramiento_id');
    }

    public function setProyectoAttribute($value)
    {
        $this->attributes['proyecto'] = strtoupper($value);
    }
}
