<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 26/09/20
 * Time: 1:08
 */

namespace Titulaciones\Models;


use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Titulaciones\Traits\FilterBy;

class Constancia extends Model
{
    use FilterBy;

    protected $table   = "constancias";
    protected $guarded = [];
    protected $casts   = [
        'configuration' => 'array',
    ];

    public function alumno()
    {
        return $this->belongsTo(Alumno::class, 'alumno_id');
    }

    public function tipo()
    {
        return $this->belongsTo(TipoConstancia::class, 'tipo_constancia_id');
    }

    public function setFechaAttribute($value)
    {
        $this->attributes['fecha'] = Carbon::createFromFormat('d/m/Y', $value)->format('Y-m-d');
    }
}
