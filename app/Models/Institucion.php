<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 13/09/20
 * Time: 20:28
 */

namespace Titulaciones\Models;


use Illuminate\Database\Eloquent\Model;

class Institucion extends Model
{
    protected $table   = 'instituciones';
    protected $guarded = [];
}
