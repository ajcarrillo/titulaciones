<?php

namespace Titulaciones\Models;

use Illuminate\Database\Eloquent\Model;
use Titulaciones\User;

class JefeDepartamento extends Model
{
    protected $table   = 'jefes_departamentos';
    protected $guarded = [];
    protected $appends = [ 'nombre_departamento' ];

    public function responsable()
    {
        return $this->belongsTo(User::class, 'responsable_id');
    }

    public function carreras()
    {
        return $this->hasMany(Carrera::class, 'jefe_departamento_id');
    }

    public function getNombreDepartamentoAttribute()
    {
        return substr($this->jefatura, 18, strlen($this->jefatura));
    }
}
