<?php

namespace Titulaciones\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class NumeroOficio extends Model
{
    protected $table    = 'numero_oficios';
    protected $fillable = [
        'numero_oficio', 'activo', 'year',
    ];

    public function nombramiento()
    {
        return $this->hasOne(Nombramiento::class, 'numero_memo_id');
    }

    public static function getNumeroOficio()
    {
        $year = Carbon::now()->year;

        if (!self::existsNumeroOficio($year)) {
            throw new \Exception('No existen número de memos');
        }

        return NumeroOficio::where('activo', 1)
            ->where('year', $year)
            ->first();
    }

    private static function existsNumeroOficio($year)
    {
        return NumeroOficio::query()
            ->where('activo', 1)
            ->where('year', $year)
            ->exists();
    }
}
