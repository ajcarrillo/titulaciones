<?php

namespace Titulaciones\Models;

use Illuminate\Database\Eloquent\Model;
use Titulaciones\Traits\FilterBy;

class Alumno extends Model
{
    use FilterBy;

    protected $table    = 'alumnos';
    protected $fillable = [
        'numero_control',
        'nombre',
        'apellido',
        'nombre_apellido',
        'sexo',
        'anio_ingreso',
        'periodo_ingreso',
        'carrera_id',
    ];
    
    public function carrera()
    {
        return $this->belongsTo(Carrera::class, 'carrera_id');
    }

    public function nombramiento()
    {
        return $this->hasOne(Nombramiento::class, 'alumno_id');
    }
}
