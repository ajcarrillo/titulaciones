<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 11/10/20
 * Time: 0:58
 */

namespace Titulaciones\Models\Filters;


use Illuminate\Database\Eloquent\Builder;
use Titulaciones\Classes\QueryFilter;

class NombramientoFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'search' => 'filled',
            'status' => 'filled',
            'year'   => 'filled',
        ];
    }

    public function search(Builder $query, $search)
    {
        $query->whereHas('alumno', function (Builder $builder) use ($search) {
            $builder->where('numero_control', 'like', "%{$search}%")
                ->orWhere('nombre_apellido', 'like', "%{$search}%");
        });
    }

    public function status(Builder $query, $status)
    {
        $query->where('estatus', $status);
    }

    public function year(Builder $query, $year)
    {
        $query->whereYear('fecha', $year);
    }
}
