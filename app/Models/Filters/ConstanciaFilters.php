<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 28/09/20
 * Time: 12:42
 */

namespace Titulaciones\Models\Filters;


use Illuminate\Database\Eloquent\Builder;
use Titulaciones\Classes\QueryFilter;

class ConstanciaFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'search' => 'filled',
            'tipo'   => 'filled',
        ];
    }

    public function search(Builder $query, $search)
    {
        $query->whereHas('alumno', function (Builder $builder) use ($search) {
            $builder->where('numero_control', 'like', "%{$search}%")
                ->orWhere('nombre_apellido', 'like', "%{$search}%");
        });
    }

    public function tipo(Builder $query, $tipo)
    {
        $query->where('tipo_constancia_id', $tipo);
    }
}
