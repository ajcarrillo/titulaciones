<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 26/02/21
 * Time: 22:57
 */

namespace Titulaciones\Models\Filters;


use Illuminate\Database\Eloquent\Builder;
use Titulaciones\Classes\QueryFilter;

class MaestroFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'search' => 'filled',
        ];
    }

    public function search(Builder $query, $search)
    {
        $query->where('nombre_completo', 'like', '%' . $search . '%')
            ->orWhere('cedula_profesional', 'like', '%' . $search . '%');
    }
}
