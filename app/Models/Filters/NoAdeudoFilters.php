<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 28/02/21
 * Time: 19:40
 */

namespace Titulaciones\Models\Filters;


use Illuminate\Database\Eloquent\Builder;
use Titulaciones\Classes\QueryFilter;

class NoAdeudoFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'search' => 'filled',
        ];
    }


    public function search(Builder $query, $search)
    {
        $query->whereHas('alumno', function (Builder $builder) use ($search) {
            $builder->where('numero_control', 'like', "%{$search}%")
                ->orWhere('nombre_apellido', 'like', "%{$search}%");
        });
    }
}
