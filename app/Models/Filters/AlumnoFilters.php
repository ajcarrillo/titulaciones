<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/09/20
 * Time: 22:36
 */

namespace Titulaciones\Models\Filters;


use Illuminate\Database\Eloquent\Builder;
use Titulaciones\Classes\QueryFilter;

class AlumnoFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'search' => 'filled',
        ];
    }

    public function search(Builder $query, $search)
    {
        $query->where('numero_control', 'like', '%' . $search . '%')
            ->orWhere('nombre_apellido', 'like', '%' . $search . '%');
    }
}
