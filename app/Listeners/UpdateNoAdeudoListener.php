<?php

namespace Titulaciones\Listeners;

use Titulaciones\Events\UpdateNoAdeudo;

class UpdateNoAdeudoListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param UpdateNoAdeudo $event
     * @return void
     */
    public function handle(UpdateNoAdeudo $event)
    {
        $solicitud = $event->adeudo;

        if (
            ($solicitud->adeuda_biblioteca === 0 && $solicitud->adeuda_biblioteca !== NULL) &&
            ($solicitud->adeuda_financieros === 0 && $solicitud->adeuda_financieros !== NULL) &&
            ($solicitud->adeuda_extraescolares === 0 && $solicitud->adeuda_extraescolares !== NULL) &&
            ($solicitud->adeuda_deptoacademico === 0 && $solicitud->adeuda_deptoacademico !== NULL) &&
            ($solicitud->adeuda_divisionestudios === 0 && $solicitud->adeuda_divisionestudios !== NULL) &&
            ($solicitud->adeuda_gestionvinculacion === 0 && $solicitud->adeuda_gestionvinculacion !== NULL)
        ) {
            $solicitud->update([
                'estatus' => 'NO ADEUDO',
            ]);
        }
    }
}
