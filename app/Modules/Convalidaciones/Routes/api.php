<?php

use Convalidaciones\Http\Controllers\Api\AsignaturaController;
use Convalidaciones\Http\Controllers\Api\ConvalidacionAsignaturaController;
use Convalidaciones\Http\Controllers\Api\ConvalidacionCarreraController;
use Convalidaciones\Http\Controllers\Api\UpdateDepartamentoConvalidacionController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([ 'as' => 'convalidaciones::api::' ], function () {
    /*Route::middleware('auth:api')->get('/convalidaciones', function (Request $request) {
        return $request->user();
    });*/

    Route::prefix('asignaturas')
        ->name('asignaturas.')
        ->group(function () {
            Route::get('/', [ AsignaturaController::class, 'index' ])->name('index');
            Route::post('importar', 'Api\ImportAsignaturaController')->name('importar');
        });

    Route::prefix('convalidaciones-carreras')
        ->name('convalidaciones.carreras.')
        ->group(function () {
            Route::get('/', [ ConvalidacionCarreraController::class, 'index' ])->name('index');
            Route::post('/', [ ConvalidacionCarreraController::class, 'store' ])->name('store');
            Route::get('{convalidacion}/', [ ConvalidacionCarreraController::class, 'show' ])->name('show');

            Route::prefix('{convalidacion}/')
                ->name('asignaturas.')
                ->group(function () {
                    Route::get('asignaturas', [ ConvalidacionAsignaturaController::class, 'index' ])->name('index');
                    Route::post('asignaturas', [ ConvalidacionAsignaturaController::class, 'store' ])->name('store');
                    Route::post('asignaturas/{asignatura}/departamento', [ UpdateDepartamentoConvalidacionController::class, 'update' ])->name('departamento.update');
                });
        });
});

