<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Convalidaciones\Http\Controllers\AnalisisController;
use Convalidaciones\Http\Controllers\CarreraConvalidadaController;
use Convalidaciones\Http\Controllers\ConvalidacionesController;
use Convalidaciones\Http\Controllers\GenerarDictamenConvalidacion;
use Convalidaciones\Http\Controllers\GuardarCalificacionController;
use Convalidaciones\Http\Controllers\SolicitudController;

Route::prefix('convalidaciones')
    ->name('convalidaciones::')
    ->middleware([ 'auth' ])
    ->group(function () {

        Route::get('/', [ ConvalidacionesController::class, 'index' ]);

        Route::get('/app/{any?}', 'SpaController')
            ->where('any', '.*');

        Route::get('/analisis', [ AnalisisController::class, 'index']);

        Route::prefix('solicitudes')
            ->name('solicitudes.')
            ->middleware([ 'auth' ])
            ->group(function () {
                Route::get('/', [ SolicitudController::class, 'index' ])->name('index');
                Route::post('/', [ SolicitudController::class, 'store' ])->name('store');
                Route::get('crear', [ SolicitudController::class, 'create' ])->name('create');
                Route::get('export', [ SolicitudController::class, 'export' ])->name('export');
                Route::get('search-alumnos', [ SolicitudController::class, 'searchAlumnos' ])->name('search.alumnos');
                Route::delete('{solicitud}', [ SolicitudController::class, 'destroy' ])->name('destroy');
                Route::post('{solicitud}/detalles/calificaciones', [ GuardarCalificacionController::class, 'store' ])->name('calificaion.store');
                Route::get('{solicitud}/editar', [ SolicitudController::class, 'edit' ])->name('edit');
                Route::get('{solicitud}/generar-dictamen', [ GenerarDictamenConvalidacion::class, 'download' ])->name('generar.dictamen');
            });

        Route::prefix('carreras')
            ->name('carreras.')
            ->group(function () {
                Route::get('/', [ CarreraConvalidadaController::class, 'index' ])->name('index');
            });
    });


