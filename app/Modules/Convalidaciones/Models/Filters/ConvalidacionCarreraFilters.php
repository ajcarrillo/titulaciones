<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 18/09/20
 * Time: 15:29
 */

namespace Convalidaciones\Models\Filters;


use Illuminate\Database\Eloquent\Builder;
use Titulaciones\Classes\QueryFilter;

class ConvalidacionCarreraFilters extends QueryFilter
{

    public function rules(): array
    {
        return [
            'carrera_origen'  => 'filled',
            'carrera_destino' => 'filled',
        ];
    }

    public function carreraOrigen(Builder $query, $carrera_origen)
    {
        $query->whereHas('carreraOrigen', function (Builder $builder) use ($carrera_origen) {
            $builder->where('descripcion', 'like', "%{$carrera_origen}%")
                ->orWhere('clave_oficial', 'like', "%{$carrera_origen}%");
        });
    }

    public function carreraDestino(Builder $query, $carrera_destino)
    {
        $query->whereHas('carreraDestino', function (Builder $builder) use ($carrera_destino) {
            $builder->where('descripcion', 'like', "%{$carrera_destino}%")
                ->orWhere('clave_oficial', 'like', "%{$carrera_destino}%");
        });
    }
}
