<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 16/09/20
 * Time: 18:12
 */

namespace Convalidaciones\Models;


use Illuminate\Database\Eloquent\Model;

class SolicitudDetalle extends Model
{
    protected $table   = 'solicitud_convalidacion_detalles';
    protected $guarded = [];

    public function asignatura()
    {
        return $this->belongsTo(Asignatura::class, 'asignatura_id');
    }

    public function solictud()
    {
        return $this->belongsTo(Solicitud::class, 'solicitud_convalidacion_id');
    }
}
