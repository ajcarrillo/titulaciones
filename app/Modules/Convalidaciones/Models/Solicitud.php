<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 13/09/20
 * Time: 22:57
 */

namespace Convalidaciones\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Titulaciones\Models\Alumno;
use Titulaciones\Models\Carrera;
use Titulaciones\Models\Institucion;

class Solicitud extends Model
{
    use SoftDeletes;

    const PENDIENTE = 1;
    const ENVIADO   = 2;
    const CONCLUIDO = 3;

    protected $table   = 'solicitudes_convalidacion';
    protected $guarded = [];
    protected $appends = [ 'status' ];
    protected $dates   = [ 'deleted_at' ];

    public function getStatusAttribute()
    {
        switch ($this->status_id) {
            case '1':
                return "PENDIENTE";
            case '2':
                return "ENVIADO";
            case '3':
                return "CONCLUIDO";
            default:
                return "SIN ESTATUS";
        }
    }

    public function alumno()
    {
        return $this->belongsTo(Alumno::class, 'alumno_id');
    }

    public function carreraDestino()
    {
        return $this->belongsTo(Carrera::class, 'carrera_destino_id');
    }

    public function institucionOrigen()
    {
        return $this->belongsTo(Institucion::class, 'institucion_origen_id');
    }

    public function institucionDestino()
    {
        return $this->belongsTo(Institucion::class, 'institucion_destino_id');
    }

    public function detalles()
    {
        return $this->hasMany(SolicitudDetalle::class, 'solicitud_convalidacion_id');
    }

    public function scopeFilter($query, array $filters)
    {
        $query->when($filters['search'] ?? NULL, function ($query, $search) {
            $query->whereHas('alumno', function (Builder $builder) use ($search) {
                $builder->where(function ($query) use ($search) {
                    $query->where('numero_control', 'like', '%' . $search . '%')
                        ->orWhere('nombre_apellido', 'like', '%' . $search . '%');
                });
            });
        });

        $query->when($filters['with_trashed'] ?? NULL, function ($query){
            $query->withTrashed();
        });
    }
}
