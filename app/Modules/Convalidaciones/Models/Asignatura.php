<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/09/20
 * Time: 14:00
 */

namespace Convalidaciones\Models;


use Illuminate\Database\Eloquent\Model;
use Titulaciones\Models\Carrera;

class Asignatura extends Model
{
    protected $table   = 'asignaturas';
    protected $guarded = [];

    public function carrera()
    {
        return $this->belongsTo(Carrera::class, 'carrera_id');
    }
}
