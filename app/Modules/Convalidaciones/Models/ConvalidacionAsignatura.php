<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 06/09/20
 * Time: 11:46
 */

namespace Convalidaciones\Models;


use Illuminate\Database\Eloquent\Model;
use Titulaciones\Models\JefeDepartamento;

class ConvalidacionAsignatura extends Model
{
    protected $table   = 'convalidaciones_asignaturas';
    protected $guarded = [];
    protected $casts   = [
        'asignaturas_cursadas' => 'array',
    ];

    public function asignaturaConvalidada()
    {
        return $this->belongsTo(Asignatura::class, 'asignatura_convalidar_id');
    }

    public function convalidacionCarrera()
    {
        return $this->belongsTo(ConvalidacionCarrera::class, 'convalidacion_carrera_id');
    }

    public function departamento()
    {
        return $this->belongsTo(JefeDepartamento::class, 'departamento_id');
    }
}
