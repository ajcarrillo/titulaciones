<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 06/09/20
 * Time: 11:46
 */

namespace Convalidaciones\Models;


use Illuminate\Database\Eloquent\Model;
use Titulaciones\Models\Carrera;
use Titulaciones\Traits\FilterBy;

class ConvalidacionCarrera extends Model
{
    use FilterBy;

    protected $table   = 'convalidaciones_carreras';
    protected $guarded = [];

    public function carreraOrigen()
    {
        return $this->belongsTo(Carrera::class, 'carrera_origen_id');
    }

    public function carreraDestino()
    {
        return $this->belongsTo(Carrera::class, 'carrera_destino_id');
    }

    public function asignaturasConvalidadas()
    {
        return $this->hasMany(ConvalidacionAsignatura::class, 'convalidacion_carrera_id');
    }
}
