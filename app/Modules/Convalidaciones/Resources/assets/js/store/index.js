import Vue from "vue"
import Vuex from "vuex"
import ConvalidacionAsignaturas from "./modules/convalidacion_asignaturas/store"

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    convasigna: ConvalidacionAsignaturas
  }
})

export default store
