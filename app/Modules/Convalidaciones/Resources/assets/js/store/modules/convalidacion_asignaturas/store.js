export default {
  namespaced: true,
  state: {
    carreraOrigen: { empty: true },
    carreraDestino: { empty: true },
    asignaturasOrigen: [],
    asignaturasDestino: [],
  },
  actions: {
    resetStore({ commit }) {
      commit("RESET_STORE")
    },
    setCarreraOrigen({ commit }, payload) {
      commit("SET_CARRERA_ORIGEN", payload)
    },
    setCarreraDestino({ commit }, payload) {
      commit("SET_CARRERA_DESTINO", payload)
    },
    setAsignaturasOrigen({ commit }, payload) {
      commit("SET_ASIGNATURAS_ORIGEN", payload)
    },
    setAsignaturasDestino({ commit }, payload) {
      commit("SET_ASIGNATURAS_DESTINO", payload)
    },
  },
  mutations: {
    RESET_STORE(state) {
      state.carreraOrigen = { empty: true }
      state.carreraDestino = { empty: true }
      state.asignaturasOrigen = []
      state.asignaturasDestino = []
    },
    SET_CARRERA_ORIGEN(state, payload) {
      state.carreraOrigen = payload
    },
    SET_CARRERA_DESTINO(state, payload) {
      state.carreraDestino = payload
    },
    SET_ASIGNATURAS_ORIGEN(state, payload) {
      state.asignaturasOrigen = payload
    },
    SET_ASIGNATURAS_DESTINO(state, payload) {
      state.asignaturasDestino = payload
    },
  },
  getters: {
    getAsignaturasOrigenBySemestre: (state) => (semestre) => {
      return state.carreraOrigen.asignaturas.filter(el => {
        return el.semestre === semestre
      })
    },
    getAsignaturasDestinoBySemestre: (state) => (semestre) => {
      return state.carreraDestino.asignaturas.filter(el => {
        return el.semestre === semestre
      })
    }
  }
}
