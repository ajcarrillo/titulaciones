window.swal = require("sweetalert2");
window.moment = require("moment")
window.axios = require("axios");

window.axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

try {
  window.$ = window.jQuery = require("jquery");
  require("bootstrap");
} catch (e) {
}
