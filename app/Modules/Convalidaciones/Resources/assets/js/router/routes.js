/*const App = () => import(/!* webpackChunkName: "modules/convalidaciones/App" *!/"../views/App")
const Home = () => import(/!* webpackChunkName: "modules/convalidaciones/Home" *!/"../views/Home")
const NotFound = () => import(/!* webpackChunkName: "modules/convalidaciones/NotFound" *!/"../views/NotFound")
const AsignaturasIndex = () => import(/!* webpackChunkName: "modules/convalidaciones/AsignaturasIndex" *!/"../views/Asignaturas/Index")
const AsignaturasImport = () => import(/!* webpackChunkName: "modules/convalidaciones/AsignaturasImport" *!/"../views/Asignaturas/Import")*/
import App from "../views/App"
import Home from "../views/Home"
import NotFound from "../views/NotFound"
import AsignaturasIndex from "../views/Asignaturas/Index"
import AsignaturasImport from "../views/Asignaturas/Import"
import ConvalidacionCarreras from "../views/ConvalidacionCarreras/Index"
import ConvalidacionAsignaturaCreate from "../views/ConvalidacionAsignaturas/Create"

export default {
  mode: "history",
  routes: [
    {
      path: "*",
      component: NotFound
    },
    {
      path: "/convalidaciones/app",
      component: App,
      redirect: "/convalidaciones/app/inicio",
      name: "home",
      children: [
        {
          path: "inicio",
          component: Home,
          name: "convalidaciones-home"
        },
        {
          path: "asignaturas",
          component: Home,
          children: [
            {
              path: "",
              component: AsignaturasIndex,
              name: "asignaturas-index",
            },
            {
              path: "importar",
              component: AsignaturasImport,
              name: "asignaturas-import",
            },
          ]
        },
        {
          path: "carreras",
          component: Home,
          children: [
            {
              path: "",
              component: ConvalidacionCarreras,
              name: "carreras-index"
            },
            {
              path: ":id/asignaturas",
              component: ConvalidacionAsignaturaCreate,
              name: "carreras-asignaturas-create",
              props: true
            },
          ]
        }
      ]
    }
  ]
}
