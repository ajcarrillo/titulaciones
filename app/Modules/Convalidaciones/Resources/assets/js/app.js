require("./bootstrap");

window.Vue = require("vue");

import VeeValidate from "vee-validate"
import VeeValidateEs from "./vee-validate-es"
import router from "./router"
import store from "./store"

Vue.use(VeeValidate, {
  locale: "es",
  dictionary: {
    es: { messages: VeeValidateEs }
  }
})

const app = new Vue({
  router,
  store
}).$mount("#app");
