export default {
  get() {
    return axios.get(route("convalidaciones::api::convalidaciones.carreras.index"))
  },
  store(paylaod) {
    return axios.post(route("convalidaciones::api::convalidaciones.carreras.store"), paylaod)
  },
  show(payload) {
    return axios.get(route("convalidaciones::api::convalidaciones.carreras.show", { convalidacion: payload }))
  }
}
