export default {
  index(payload) {
    return axios.get(route("convalidaciones::api::convalidaciones.carreras.asignaturas.index", { convalidacion: payload }))
  },
  store(payload) {
    let { convalidacion_carrera_id } = payload
    return axios.post(route("convalidaciones::api::convalidaciones.carreras.asignaturas.store", { convalidacion: convalidacion_carrera_id }), payload)
  }
}
