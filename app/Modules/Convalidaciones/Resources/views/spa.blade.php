@extends('convalidaciones::layouts.master')

@section('content')
	<router-view></router-view>
@endsection

@section('extra-script-before')
	<script type="text/javascript">
      window.carreras = {!! json_encode($carreras) !!}
        window.departamentos = {!! json_encode($departamentos) !!}
	</script>
@endsection
