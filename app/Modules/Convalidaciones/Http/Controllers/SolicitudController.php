<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 13/09/20
 * Time: 22:56
 */

namespace Convalidaciones\Http\Controllers;


use Convalidaciones\Models\ConvalidacionCarrera;
use Convalidaciones\Models\Solicitud;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Writer\Exception;
use Redirect;
use Titulaciones\Exports\SolicitudConvalidacionExport;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\Alumno;
use Titulaciones\Models\Carrera;
use Titulaciones\Models\Institucion;
use URL;

class SolicitudController extends Controller
{
    protected $root = 'Convalidaciones/Solicitudes';

    public function index(Request $request)
    {
        $solicitudes = Solicitud::query()
            ->filter($request->only('search', 'with_trashed'))
            ->with('alumno', 'alumno.carrera', 'carreraDestino', 'detalles')
            ->get();

        return Inertia::render("{$this->root}/Index", [
            'filters'     => $request->all('search', 'with_trashed'),
            'solicitudes' => $solicitudes,
            'create_url'  => URL::route('convalidaciones::solicitudes.create'),
        ]);
    }

    public function create(Request $request)
    {
        $instituciones = Institucion::query()
            ->orderBy('descripcion')
            ->get();

        $carreras = Carrera::query()
            ->orderBy('descripcion')
            ->where('estado', '=', 'Vigente')
            ->where('nivel_escolar', '!=', 'P')
            ->where('tipo', '!=', 'AC')
            ->get();

        return Inertia::render("{$this->root}/Create", [
            'instituciones' => $instituciones,
            'carreras'      => $carreras,
        ]);
    }

    public function store(Request $request)
    {
        $item = new Solicitud($request->input());

        $item->save();

        return Redirect::route('convalidaciones::solicitudes.edit', [ 'solicitud' => $item->id ]);
    }

    public function edit(Solicitud $solicitud)
    {
        $solicitud->load('alumno', 'alumno.carrera', 'carreraDestino', 'detalles');

        $convalidacion = $this->getConvalidacion($solicitud->alumno->carrera_id, $solicitud->carrera_destino_id);

        return Inertia::render("$this->root/Edit", [
            'solicitud'     => $solicitud,
            'convalidacion' => $convalidacion,
        ]);
    }

    public function update()
    {

    }

    public function destroy($solicitud)
    {
        Solicitud::query()
            ->find($solicitud)
            ->delete();

        return Redirect::route('convalidaciones::solicitudes.index');
    }

    public function export()
    {
        try {
            return Excel::download(new SolicitudConvalidacionExport, 'solicitudes.xlsx');
        } catch (Exception $e) {
            return [ 'errors' => [ $e->getMessage() ] ];
        } catch (\PhpOffice\PhpSpreadsheet\Exception $e) {
            return [ 'errors' => [ $e->getMessage() ] ];
        }
    }

    public function searchAlumnos(Request $request)
    {
        $term = $request->query('term');

        $alumnos = Alumno::query()
            ->with('carrera')
            ->orderBy('nombre_apellido')
            ->where('numero_control', 'like', "%{$term}%")
            ->orWhere('nombre_apellido', 'like', "%{$term}%")
            ->get();

        return ok(compact('alumnos'));
    }

    protected function getConvalidacion($carreraOrigen, $carreraDestino)
    {
        return ConvalidacionCarrera::query()
            ->with('asignaturasConvalidadas', 'asignaturasConvalidadas.asignaturaConvalidada:id,nombre_materia')
            ->where('carrera_origen_id', $carreraOrigen)
            ->where('carrera_destino_id', $carreraDestino)
            ->first();
    }
}
