<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 06/09/20
 * Time: 11:49
 */

namespace Convalidaciones\Http\Controllers\Api;


use Convalidaciones\Models\ConvalidacionAsignatura;
use Convalidaciones\Models\ConvalidacionCarrera;
use DB;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Titulaciones\Http\Controllers\Controller;

class ConvalidacionCarreraController extends Controller
{
    public function index()
    {
        $items = ConvalidacionCarrera::query()
            ->with('carreraOrigen', 'carreraDestino')
            ->get();

        return ok([
            'items' => $items,
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'carrera_origen_id'  => [ 'required' ],
            'carrera_destino_id' => [ 'required', 'different:carrera_origen_id', Rule::unique('convalidaciones_carreras')
                ->where(function ($query) use ($request) {
                    return $query->where('carrera_origen_id', $request->input('carrera_origen_id'))
                        ->where('carrera_destino_id', $request->input('carrera_destino_id'));
                }) ],
        ], [
            'carrera_destino_id.different' => 'La carrera destino debe ser diferente a la carrera de origen',
            'carrera_destino_id.unique'    => 'La convalidación para esta combinación ya existe',
        ]);


        try {
            DB::beginTransaction();
            $data          = $request->input();
            $convalidacion = new ConvalidacionCarrera($data);

            $convalidacion->save();
            $convalidacion->load('carreraOrigen', 'carreraDestino');

            $idsOrigen  = $convalidacion->carreraOrigen->asignaturas()->pluck('clave_materia')->toArray();
            $idsDestino = $convalidacion->carreraDestino->asignaturas()->whereIn('clave_materia', $idsOrigen)->pluck('clave_materia')->toArray();

            foreach ($idsDestino as $clave) {
                $asignaturas_cursadas     = $convalidacion->carreraOrigen->asignaturas()->where('clave_materia', $clave)->first();
                $asignatura_convalidar_id = $convalidacion->carreraDestino->asignaturas()->where('clave_materia', $clave)->value('id');

                ConvalidacionAsignatura::create([
                    'convalidacion_carrera_id' => $convalidacion->id,
                    'asignaturas_cursadas'     => [ $asignaturas_cursadas ],
                    'asignatura_convalidar_id' => $asignatura_convalidar_id,
                    'porcentaje'               => 100,
                ]);
            }

            DB::commit();

            return ok([
                'convalidacion' => $convalidacion,
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([ 'errors' => [ $e->getMessage() ] ], 422);
        }
    }

    public function show(ConvalidacionCarrera $convalidacion)
    {
        $convalidacion->load(
            'carreraOrigen',
            'carreraOrigen.asignaturas:id,carrera_id,clave_materia,nombre_materia,creditos,semestre',
            'carreraDestino',
            'carreraDestino.asignaturas:id,carrera_id,clave_materia,nombre_materia,creditos,semestre'
        );

        return ok(compact('convalidacion'));
    }
}
