<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 12/09/20
 * Time: 11:03
 */

namespace Convalidaciones\Http\Controllers\Api;


use Convalidaciones\Models\ConvalidacionAsignatura;
use Convalidaciones\Models\ConvalidacionCarrera;
use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;

class ConvalidacionAsignaturaController extends Controller
{
    public function index(ConvalidacionCarrera $convalidacion)
    {
        $items = ConvalidacionAsignatura::query()
            ->with(
                'asignaturaConvalidada',
                'convalidacionCarrera',
                'departamento'
            )
            ->where('convalidacion_carrera_id', $convalidacion->id)
            ->get();

        return ok([
            'asignaturas' => $items,
        ]);
    }

    public function store(Request $request, ConvalidacionCarrera $convalidacion)
    {
        //$item = new ConvalidacionAsignatura($request->input());
        $convalidacion->asignaturasConvalidadas()->updateOrCreate([
            'convalidacion_carrera_id' => $convalidacion->id,
            'asignatura_convalidar_id' => $request->input('asignatura_convalidar_id'),
        ], $request->input());
    }
}
