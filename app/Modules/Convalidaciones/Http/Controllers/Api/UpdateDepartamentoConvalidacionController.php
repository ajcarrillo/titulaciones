<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 19/09/20
 * Time: 1:20
 */

namespace Convalidaciones\Http\Controllers\Api;


use Convalidaciones\Models\ConvalidacionAsignatura;
use Convalidaciones\Models\ConvalidacionCarrera;
use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;

class UpdateDepartamentoConvalidacionController extends Controller
{
    public function update(Request $request, ConvalidacionCarrera $convalidacion, ConvalidacionAsignatura $asignatura)
    {
        $asignatura->update([
            'departamento_id' => $request->input('departamento_id'),
        ]);

        return ok();
    }
}
