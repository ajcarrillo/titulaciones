<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/09/20
 * Time: 15:27
 */

namespace Convalidaciones\Http\Controllers\Api;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ImportAsignaturaController
{
    public function __invoke(Request $request)
    {
        $date = Carbon::now()->format('Y.m.d.H.i.s');

        $name      = $request->file('import_file')->getClientOriginalName();
        $filename  = mb_strtolower(pathinfo($name, PATHINFO_FILENAME), 'UTF-8');
        $extension = pathinfo($name, PATHINFO_EXTENSION);

        $request->file('import_file')->storeAs(
            'topics', "{$date}_{$filename}.{$extension}", 'imports'
        );

        return ok([
            'command' => "php artisan import:asignaturas {$date}_{$filename}.{$extension}",
        ]);
    }
}
