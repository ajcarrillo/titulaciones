<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/09/20
 * Time: 18:48
 */

namespace Convalidaciones\Http\Controllers\Api;


use Convalidaciones\Models\Asignatura;
use Titulaciones\Http\Controllers\Controller;

class AsignaturaController extends Controller
{
    public function index()
    {
        $asignaturas = Asignatura::query()
            ->join('carreras', function ($join) {
                $join->on('asignaturas.carrera_id', '=', 'carreras.id');
            })
            ->orderBy('carreras.descripcion')
            ->orderBy('carreras.clave_oficial')
            ->orderBy('asignaturas.semestre')
            ->orderBy('asignaturas.nombre_materia')
            ->paginate();

        return ok([
            'asignaturas' => $asignaturas,
        ]);
    }
}
