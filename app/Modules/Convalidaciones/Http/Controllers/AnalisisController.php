<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 13/02/21
 * Time: 11:28
 */

namespace Convalidaciones\Http\Controllers;


use Convalidaciones\Models\ConvalidacionAsignatura;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use Titulaciones\Exports\AnalisisConvalidacionExport;
use Titulaciones\Http\Controllers\Controller;

class AnalisisController extends Controller
{
    public function index(Request $request)
    {
        $query = ConvalidacionAsignatura::query()
            ->with(
                'asignaturaConvalidada',
                'convalidacionCarrera',
                'convalidacionCarrera.carreraOrigen',
                'convalidacionCarrera.carreraDestino'
            )
            ->where('convalidacion_carrera_id', $request->query('carrera_origen', 1));

        $data = $query->get();

        $rows = $data->map(function ($el) {
            $array = [];
            foreach ($el->asignaturas_cursadas as $cursadas) {
                array_push($array, [
                    'carrera_origen'        => $el->convalidacionCarrera->carreraOrigen->descripcion,
                    'clave_cursada'         => $cursadas['clave_materia'],
                    'materia_cursada'       => $cursadas['nombre_materia'],
                    'carrera_destino'       => $el->convalidacionCarrera->carreraDestino->descripcion,
                    'clave_convalidar'      => $el->asignaturaConvalidada->clave_materia,
                    'asignatura_convalidar' => $el->asignaturaConvalidada->nombre_materia,
                    'porcentaje'            => $el->porcentaje,
                ]);
            }

            return $array;
        });

        $array = [];
        foreach ($rows as $row) {
            foreach ($row as $item) {
                array_push($array, $item);
            }
        }

        $export = new AnalisisConvalidacionExport($array, array_keys(current($array)));

        $fileName = date("YmdHi");
        $origen  = Str::slug($data[0]->convalidacionCarrera->carreraOrigen->descripcion, '_');
        $destino = Str::slug($data[0]->convalidacionCarrera->carreraDestino->descripcion, '_');

        return Excel::download($export, "{$fileName}_{$origen}_a_{$destino}.xls");
    }
}
