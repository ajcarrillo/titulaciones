<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 16/09/20
 * Time: 18:44
 */

namespace Convalidaciones\Http\Controllers;


use Convalidaciones\Models\Solicitud;
use Convalidaciones\Models\SolicitudDetalle;
use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;

class GuardarCalificacionController extends Controller
{
    public function store(Request $request, Solicitud $solicitud)
    {
        $item = SolicitudDetalle::query()
            ->updateOrCreate([
                'solicitud_convalidacion_id' => $solicitud->id,
                'asignatura_id'              => $request->input('asignatura_id'),
            ], [
                'calificacion' => $request->input('calificacion'),
            ]);

        return ok([ 'item' => $item ]);
    }
}
