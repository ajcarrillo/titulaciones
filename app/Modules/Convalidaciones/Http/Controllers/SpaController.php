<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/09/20
 * Time: 13:09
 */

namespace Convalidaciones\Http\Controllers;


use DB;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\Carrera;
use Titulaciones\Models\JefeDepartamento;

class SpaController extends Controller
{
    public function __invoke()
    {
        $carreras      = $this->getCarreras();
        $departamentos = $this->getDepartamentos();

        return view('convalidaciones::spa', [
            'carreras'      => $carreras,
            'departamentos' => $departamentos,
        ]);
    }

    protected function getCarreras()
    {
        return Carrera::query()
            ->select(DB::raw("id, concat_ws('/', clave_oficial, descripcion) descripcion"))
            ->orderBy('descripcion')
            ->where('estado', 'Vigente')
            ->get();
    }

    public function getDepartamentos()
    {
        return JefeDepartamento::query()
            ->with('responsable')
            ->orderBy('jefatura')
            ->get();
    }
}
