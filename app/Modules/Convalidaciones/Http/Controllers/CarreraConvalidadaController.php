<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 18/09/20
 * Time: 14:06
 */

namespace Convalidaciones\Http\Controllers;


use Convalidaciones\Models\ConvalidacionCarrera;
use Convalidaciones\Models\Filters\ConvalidacionCarreraFilters;
use Illuminate\Http\Request;
use Inertia\Inertia;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\Carrera;

class CarreraConvalidadaController extends Controller
{
    protected $root = 'Convalidaciones/Carreras';

    public function index(Request $request, ConvalidacionCarreraFilters $filters)
    {
        $values          = $this->getValues($request);
        $items           = $this->getConvalidaciones($filters, $values);
        $carreras        = $this->getCarreras($request);
        $carrerasDestino = $this->getCarrerasDestino();

        return Inertia::render("{$this->root}/Index", [
            'filters'         => $this->getAll($request),
            'convalidaciones' => $items,
            'carreras'        => $carreras,
            'carrerasDestino' => $carrerasDestino,
        ]);
    }

    protected function getCarreras($request)
    {
        /*return Carrera::query()
            ->orderBy('descripcion')
            ->filterBy($filters, $values)
            ->where('nivel_escolar', 'L')
            ->get();*/

        $fil = $request->query('liquidadas', false);

        $query = Carrera::query()
            ->orderBy('descripcion')
            ->where('nivel_escolar', 'L');

        $query->when($fil, function ($query) {
            $query->whereIn('estado', [ 'Liquidada', 'Liquidacion' ]);
        }, function ($query) {
            $query->where('estado', 'Vigente');
        });

        return $query->get();
    }

    public function getCarrerasDestino()
    {
        return Carrera::query()
            ->where('estado', 'Vigente')
            ->where('nivel_escolar', 'L')
            ->orderBy('descripcion')
            ->get();
    }

    protected function getConvalidaciones(ConvalidacionCarreraFilters $filters, array $values)
    {
        return ConvalidacionCarrera::query()
            ->filterBy($filters, $values)
            ->with('carreraOrigen', 'carreraDestino')
            ->get();
    }

    protected function getValues(Request $request): array
    {
        return $request->only('carrera_origen', 'carrera_destino');
    }

    protected function getAll(Request $request): array
    {
        return $request->all('carrera_origen', 'carrera_destino', 'liquidadas');
    }
}
