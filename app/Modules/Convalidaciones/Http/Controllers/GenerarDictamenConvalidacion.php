<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 16/09/20
 * Time: 23:28
 */

namespace Convalidaciones\Http\Controllers;


use Convalidaciones\Models\ConvalidacionCarrera;
use Convalidaciones\Models\Solicitud;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use Titulaciones\Http\Controllers\Controller;

class GenerarDictamenConvalidacion extends Controller
{
    public function download(Request $request, Solicitud $solicitud)
    {
        /*Date::setLocale('es');
        $solicitud->load('alumno', 'alumno.carrera', 'detalles', 'carreraDestino');

        $cols                = 5;
        $fecha               = new Date($solicitud->created_at);
        $fecha               = strtoupper($fecha->format('l j F \\d\\e Y'));
        $alumno              = $solicitud->alumno->nombre_apellido;
        $carreraOrigen       = $solicitud->alumno->carrera->descripcion;
        $carreraOrigenClave  = $solicitud->alumno->carrera->clave_oficial;
        $carreraDestino      = $solicitud->carreraDestino->descripcion;
        $carreraDestinoClave = $solicitud->carreraDestino->clave_oficial;

        $convConfig = $this->getConvalidacion($solicitud->alumno->carrera->id, $solicitud->carreraDestino->id);

        $rows = $solicitud->detalles->map(function ($el) use ($convConfig) {

            foreach ($convConfig as $item) {
                $collection = collect($item['asignaturas_cursadas']);
                $filtered   = $collection->where('id', $el->asignatura_id)->first();

                if ($filtered) {
                    return [
                        'cursada_id'   => $el->asignatura_id,
                        'cursada'      => $el->asignatura->nombre_materia,
                        'calificacion' => $el->calificacion,
                        'convalida'    => $filtered ? $item->asignaturaConvalidada->nombre_materia : NULL,
                        'creditos'    => $filtered ? $item->asignaturaConvalidada->creditos : NULL,
                    ];
                } else {
                    continue;
                }
            }
        });*/


        /*$rows = collect($convConfig)->map(function ($el) use ($solicitud) {
            $cursada = SolicitudDetalle::query()
                ->where('solicitud_convalidacion_id', $solicitud->id)
                ->whereIn('asignatura_id', $el['asignaturas_cursadas'])
                ->first();

            if ($cursada) {
                return [
                    'cursadas' => $cursada,
                ];
            }
        });*/


        //return ok(compact('fecha', 'alumno', 'carreraOrigen', 'carreraOrigenClave', 'carreraDestino', 'carreraDestinoClave', 'convConfig'));
        //return compact('rows', 'convConfig');
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor(storage_path('app/machotes/dictamen_de_convalidaciones.docx'));

        Date::setLocale('es');
        $solicitud->load('alumno', 'alumno.carrera', 'detalles', 'carreraDestino');

        $cols                = 5;
        $fecha               = new Date($solicitud->created_at);
        $fecha               = strtoupper($fecha->format('j \\d\\e F \\d\\e Y'));
        $alumno              = $solicitud->alumno->nombre_apellido;
        $carreraOrigen       = $solicitud->alumno->carrera->descripcion;
        $carreraOrigenClave  = $solicitud->alumno->carrera->clave_oficial;
        $carreraDestino      = $solicitud->carreraDestino->descripcion;
        $carreraDestinoClave = $solicitud->carreraDestino->clave_oficial;

        $convConfig = $this->getConvalidacion($solicitud->alumno->carrera->id, $solicitud->carreraDestino->id);

        $rows = $solicitud->detalles->map(function ($el) use ($convConfig) {
            foreach ($convConfig as $item) {
                $collection = collect($item['asignaturas_cursadas']);
                $filtered   = $collection->where('id', $el->asignatura_id)->first();

                if ($filtered) {
                    return [
                        'cursada_id'   => $el->asignatura_id,
                        'cursada'      => $el->asignatura->nombre_materia,
                        'calificacion' => $el->calificacion == 0 ? 'N/A' : $el->calificacion,
                        'convalida'    => $filtered ? $item->asignaturaConvalidada->nombre_materia : NULL,
                        'creditos'     => $filtered ? $item->asignaturaConvalidada->creditos : NULL,
                    ];
                } else {
                    continue;
                }
            }
        });

        $rows  = $rows->toArray();
        $clone = [];

        foreach ($rows as $row) {
            if ($row !== NULL) {
                array_push($clone, $row);
            }
        }

        $templateProcessor->setValue('fecha', $fecha);
        $templateProcessor->setValue('alumno', $alumno);
        $templateProcessor->setValue('carrera_origen', $carreraOrigen);
        $templateProcessor->setValue('clave_origen', $carreraOrigenClave);
        $templateProcessor->setValue('carrera_destino', $carreraDestino);
        $templateProcessor->setValue('clave_destino', $carreraDestinoClave);
        $templateProcessor->cloneRowAndSetValues('cursada', $clone);

        $templateProcessor->saveAs(storage_path("app/public/convalidaciones/dictamen_solicitud_{$solicitud->alumno->numero_control}.docx"));

        return response()->download(storage_path("app/public/convalidaciones/dictamen_solicitud_{$solicitud->alumno->numero_control}.docx"));
    }

    protected function getConvalidacion($carreraOrigen, $carreraDestino)
    {
        $convalidacion = ConvalidacionCarrera::query()
            ->with([ 'asignaturasConvalidadas' => function ($query) {
                $query->with('asignaturaConvalidada:id,nombre_materia,creditos')
                    ->where('porcentaje', '>=', 60);
            } ])
            ->where('carrera_origen_id', $carreraOrigen)
            ->where('carrera_destino_id', $carreraDestino)
            ->first();

        return $convalidacion->asignaturasConvalidadas;
    }
}
