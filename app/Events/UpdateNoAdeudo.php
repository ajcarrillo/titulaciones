<?php

namespace Titulaciones\Events;

use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Titulaciones\Models\NoAdeudo;

class UpdateNoAdeudo
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var NoAdeudo
     */
    public $adeudo;

    public function __construct(NoAdeudo $adeudo)
    {
        //
        $this->adeudo = $adeudo;
    }
}
