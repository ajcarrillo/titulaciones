<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 13/02/21
 * Time: 15:48
 */

namespace Titulaciones\Exports;


use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\WithHeadings;

class AnalisisConvalidacionExport implements FromArray, WithHeadings
{
    /**
     * @var array
     */
    private $rows;
    /**
     * @var array
     */
    private $headers;

    public function __construct(array $rows, array $headers)
    {
        $this->rows    = $rows;
        $this->headers = $headers;
    }

    public function array(): array
    {
        return $this->rows;
    }

    public function headings(): array
    {
        return $this->headers;
    }
}
