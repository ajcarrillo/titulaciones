<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 08/06/18
 * Time: 13:05
 */

namespace Titulaciones\Exports;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Titulaciones\Models\Nombramiento;

class NombramientosExports implements FromCollection, WithHeadings, WithMapping
{
    public function __construct($del, $al)
    {
        $this->del = $del;
        $this->al  = $al;
    }

    public function headings(): array
    {
        return [
            'FECHA',
            'HORA',
            'NUMERO_CONTROL',
            'GRADO',
            'NOMBRE',
            'SEXO',
            'CARRERA',
            'OPCION',
            'MODULO',
            'PROYECTO',
            'PRESIDENTE',
            'SECRETARIO',
            'VOCAL',
            'VOCAL_SUPLENTE',
        ];
    }

    public function collection()
    {
        /*$query = Nombramiento::query();

        $query->join('alumnos as a', 'nombramientos.alumno_id', '=', 'a.id')
            ->join('carreras as c', 'a.carrera_id', '=', 'c.id')
            ->join('opciones_titulacion as o', 'nombramientos.opcion_id', '=', 'o.id')
            ->leftJoin('modulos_titulacion as m', 'nombramientos.modulo_id', '=', 'm.id')
            ->join('horarios as h', 'nombramientos.horario_id', '=', 'h.id')
            ->orderBy('fecha', 'ASC')
            ->orderBy('horario_id', 'ASC')
            ->where('estatus', 'C')
            ->whereBetween('fecha', [ $this->del, $this->al ])
            ->selectRaw('nombramientos.fecha')
            ->selectRaw('h.hora')
            ->selectRaw('a.numero_control')
            ->selectRaw('c.grado')
            ->selectRaw('a.nombre_apellido')
            ->selectRaw('a.sexo')
            ->selectRaw('c.descripcion as carrera')
            ->selectRaw('o.descripcion as opcion')
            ->selectRaw('m.descripcion as modulo')
            ->selectRaw('nombramientos.proyecto');


        return $query->get();*/
        $query = Nombramiento::query()
            ->with('alumno:id,numero_control,nombre_apellido,sexo,carrera_id',
                'alumno.carrera:id,descripcion,grado',
                'opcion:id,descripcion',
                'modulo:id,descripcion',
                'horario:id,hora',
                'sinodales',
                'sinodales.maestro')
            ->where('estatus', 'C')
            ->whereBetween('fecha', [ $this->del, $this->al ]);

        return $query->get();
    }

    public function map($row): array
    {
        $fecha = Carbon::parse("{$row->fecha}")->format('Y-m-d');

        return [
            Carbon::parse("{$row->fecha}")->format('d/m/Y'),
            Carbon::parse("{$fecha} {$row->horario->hora}")->format('H:i'),
            $row->alumno->numero_control,
            $row->alumno->carrera->grado,
            $row->alumno->nombre_apellido,
            $row->alumno->sexo,
            $row->alumno->carrera->descripcion,
            $row->opcion->descripcion,
            $row->modulo->empty ? 'N/A' : $row->modulo->descripcion,
            $row->proyecto,
            $row->sinodales()->where('tipo', 'PRESIDENTE')->first()->maestro->nombre_completo,
            $row->sinodales()->where('tipo', 'SECRETARIO')->first()->maestro->nombre_completo,
            $row->sinodales()->where('tipo', 'VOCAL')->first()->maestro->nombre_completo,
            $row->sinodales()->where('tipo', 'VOCAL SUPLENTE')->first()->maestro->nombre_completo,
        ];
    }

}
