<?php


namespace Titulaciones\Exports;


use Jenssegers\Date\Date;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Titulaciones\Models\Nombramiento;

class ActasExports implements FromCollection, WithHeadings, WithMapping
{
    private $del;
    private $al;

    public function __construct($del, $al)
    {
        $this->del = $del;
        $this->al  = $al;
    }

    public function headings(): array
    {
        return [
            'NOMBRE',
            'NUM_CONTROL',
            'CARRERA',
            'OPCIÓN DE TITULACIÓN',
            'NOMBRE DEL PROYECTO',
            'FECHA',
            'MES',
            'PRESIDENTE',
            'CEDULA',
            'SECRETARIO',
            'CEDULA',
            'VOCAL',
            'CEDULA',
            'LINK DE VIDEO'
        ];
    }

    public function collection()
    {
        $query = Nombramiento::query()
            ->with('alumno:id,numero_control,nombre_apellido,sexo,carrera_id',
                'alumno.carrera:id,descripcion,grado',
                'opcion:id,descripcion',
                'modulo:id,descripcion',
                'horario:id,hora',
                'sinodales',
                'sinodales.maestro')
            ->where('estatus', 'C')
            ->whereBetween('fecha', [$this->del, $this->al])
            ->orderBy('fecha')
            ->orderBy('horario_id');

        return $query->get();
    }

    public function map($row): array
    {
        Date::setLocale('es');
        $fecha = new Date($row->fecha);

        $fechaDia   = $fecha->format('d');
        $fechaMes   = strtoupper($fecha->format('F'));
        $modulo     = $row->modulo->empty ? 'N/A' : $row->modulo->descripcion;
        $presidente = $row->sinodales()->where('tipo', 'PRESIDENTE')->first();
        $secretario = $row->sinodales()->where('tipo', 'SECRETARIO')->first();
        $vocal      = $row->sinodales()->where('tipo', 'VOCAL')->first();

        return [
            $row->alumno->nombre_apellido,
            $row->alumno->numero_control,
            $row->alumno->carrera->descripcion,
            "{$row->opcion->descripcion} ({$modulo})",
            $row->proyecto,
            $fechaDia,
            $fechaMes,
            $presidente->maestro->nombre_completo,
            $presidente->maestro->cedula_profesional,
            $secretario->maestro->nombre_completo,
            $secretario->maestro->cedula_profesional,
            $vocal->maestro->nombre_completo,
            $vocal->maestro->cedula_profesional,
            $row->video_link,
        ];
    }
}