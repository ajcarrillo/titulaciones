<?php

namespace Titulaciones\Exports;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Titulaciones\Models\Nombramiento;

class AnexoTresComputo implements FromCollection, WithHeadings, WithMapping
{

    public function __construct($del, $al)
    {
        $this->del = $del;
        $this->al  = $al;
    }

    public function collection()
    {
        $query = Nombramiento::query()
            ->with('alumno:id,numero_control,nombre_apellido,sexo,carrera_id',
                'alumno.carrera:id,descripcion,grado',
                'opcion:id,descripcion',
                'modulo:id,descripcion',
                'horario:id,hora',
                'sinodales',
                'sinodales.maestro')
            ->where('estatus', 'C')
            ->whereBetween('fecha', [ $this->del, $this->al ]);

        return $query->get();
    }

    public function headings(): array
    {
        return [
            'FECHA',
            'HORA',
            'NUMERO_CONTROL',
            'GRADO',
            'NOMBRE',
            'SEXO',
            'CARRERA',
            'OPCION',
            'MODULO',
            'PROYECTO',
            'PRESIDENTE',
            'PRESIDENTE_EMAIL',
            'PRESIDENTE_TELEFONO',
            'SECRETARIO',
            'SECRETARIO_EMAIL',
            'SECRETARIO_TELEFONO',
            'VOCAL',
            'VOCAL_EMAIL',
            'VOCAL_TELEFONO',
            'VOCAL_SUPLENTE',
            'VOCAL_SUPLENTE_EMAIL',
            'VOCAL_SUPLENTE_TELEFONO',
            'REPRESENTANTE_DIVISION',
        ];
    }

    public function map($row): array
    {
        $fecha = Carbon::parse("{$row->fecha}")->format('Y-m-d');

        $presidente    = $row->sinodales()->where('tipo', 'PRESIDENTE')->first();
        $secretario    = $row->sinodales()->where('tipo', 'SECRETARIO')->first();
        $vocal         = $row->sinodales()->where('tipo', 'VOCAL')->first();
        $vocalSuplente = $row->sinodales()->where('tipo', 'VOCAL SUPLENTE')->first();


        return [
            Carbon::parse("{$row->fecha}")->format('d/m/Y'),
            Carbon::parse("{$fecha} {$row->horario->hora}")->format('H:i'),
            $row->alumno->numero_control,
            $row->alumno->carrera->grado,
            $row->alumno->nombre_apellido,
            $row->alumno->sexo,
            $row->alumno->carrera->descripcion,
            $row->opcion->descripcion,
            $row->modulo->empty ? 'N/A' : $row->modulo->descripcion,
            $row->proyecto,
            $presidente->maestro->nombre_completo,
            $presidente->maestro->email,
            $presidente->maestro->telefono,
            $secretario->maestro->nombre_completo,
            $secretario->maestro->email,
            $secretario->maestro->telefono,
            $vocal->maestro->nombre_completo,
            $vocal->maestro->email,
            $vocal->maestro->telefono,
            $vocalSuplente->maestro->nombre_completo,
            $vocalSuplente->maestro->email,
            $vocalSuplente->maestro->telefono,
            $row->representante_division,
        ];
    }
}