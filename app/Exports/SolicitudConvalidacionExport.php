<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/09/20
 * Time: 16:49
 */

namespace Titulaciones\Exports;


use Carbon\Carbon;
use Convalidaciones\Models\Solicitud;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class SolicitudConvalidacionExport implements FromCollection, WithHeadings, WithMapping
{

    public function collection()
    {
        $query = Solicitud::query()
            ->with(
                'alumno',
                'alumno.carrera',
                'carreraDestino'
            );

        return $query->get();
    }

    public function headings(): array
    {
        return [
            'FECHA',
            'NUM CONTROL',
            'ALUMNO',
            'CARRERA ORIGEN',
            'CARRERA DESTINO',
        ];
    }

    public function map($row): array
    {
        return [
            Carbon::parse($row->created_at)->format('d/m/Y'),
            $row->alumno->numero_control,
            $row->alumno->nombre_apellido,
            $row->alumno->carrera->descripcion_completa,
            $row->carreraDestino->descripcion_completa,
        ];
    }
}
