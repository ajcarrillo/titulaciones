<?php

namespace Titulaciones;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Titulaciones\Models\JefeDepartamento;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'api_token',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token',
    ];

    protected $appends = [ 'selected_roles' ];

    public function departamento()
    {
        return $this->hasOne(JefeDepartamento::class, 'responsable_id');
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user');
    }

    public function getSelectedRolesAttribute()
    {
        return \DB::table('role_user')
            ->where('user_id', $this->id)
            ->pluck('role_id');
    }

    public function enabled()
    {
        $this->update([
            'active' => 1,
        ]);
    }

    public function disabled()
    {
        $this->update([
            'active' => 0,
        ]);
    }

    public function hasRole($role)
    {
        if ( ! is_array($role)) {
            return $this->roles()->where('descripcion', $role)->count();
        }
        $rolesDelUsuario = $this->roles->pluck('descripcion')->toArray();

        if (count(array_intersect($rolesDelUsuario, $role))) {
            return true;
        }

        return false;
    }
}
