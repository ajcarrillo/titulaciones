<?php

namespace Titulaciones\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Titulaciones\Imports\AsignaturasImport;

class ImportAsignaturasCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'import:asignaturas';

    protected $signature = 'import:asignaturas {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa asignaturas al catálogo.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = storage_path("app/imports/topics/{$this->argument('file')}");
        $this->info("Importing students in file {$this->argument('file')}...");
        Excel::import(new AsignaturasImport, $file, NULL, \Maatwebsite\Excel\Excel::XLS);
        $this->info('Imported topics successfully');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            [ 'example', InputArgument::REQUIRED, 'An example argument.' ],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [
            [ 'example', NULL, InputOption::VALUE_OPTIONAL, 'An example option.', NULL ],
        ];
    }
}
