<?php

namespace Titulaciones\Console\Commands;

use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;
use Titulaciones\Imports\AlumnosImport;

class ImportAlumnosXLS extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:alumnos {file}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Importa alumnos contenidos en un archivo de Excel (.xls)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $file = storage_path("app/imports/students/{$this->argument('file')}");
        $this->info("Importing students in file {$this->argument('file')}...");
        Excel::import(new AlumnosImport, $file, NULL, \Maatwebsite\Excel\Excel::XLS);
        $this->info('Imported students successfully');
    }
}
