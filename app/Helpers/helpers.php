<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/06/18
 * Time: 11:09
 */
function currentUser()
{
    $user = Auth::user();

    return $user;
}

/**
 * @return \Titulaciones\User
 */
function currentAPIUser()
{
    $user = Auth::guard('api')->user();

    return $user;
}

