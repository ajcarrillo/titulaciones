<?php

namespace Titulaciones\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Titulaciones\Models\Carrera;

class CarrerasImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        \Log::info($row);
        \Log::info($row['carrera']);

        return new Carrera([
            'sii_id'        => trim($row['carrera']),
            'descripcion'   => trim($row['nombre']),
            'clave_oficial' => trim($row['claveoficial']),
            'estado'        => trim($row['estado']),
            'tipo'          => trim($row['tipo']),
            'nivel_escolar' => trim($row['nivelescolar']),
            'reticula'      => trim($row['reticula']),
        ]);
    }
}
