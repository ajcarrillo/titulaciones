<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 05/09/20
 * Time: 15:13
 */

namespace Titulaciones\Imports;


use Convalidaciones\Models\Asignatura;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Titulaciones\Models\Carrera;

class AsignaturasImport implements ToModel, WithHeadingRow, WithChunkReading
{

    public function model(array $row)
    {
        $carreraId = trim($row['carrera']);
        $reticula  = trim($row['reticula']);
        $carrera   = $this->getCarrera($carreraId, $reticula);

        return new Asignatura([
            'carrera_id'     => $carrera->id,
            'clave_materia'  => trim($row['clavemateria']),
            'nombre_materia' => trim($row['nombremateria']),
            'creditos'       => trim($row['creditos']),
            'semestre'       => str_pad($row['semestre'], 2, "0", STR_PAD_LEFT),
        ]);
    }

    protected function getCarrera($carreraId, $reticula)
    {
        return Carrera::query()
            ->where('sii_id', $carreraId)
            ->where('reticula', $reticula)
            ->first();
    }

    public function chunkSize(): int
    {
        return 1000;
    }
}
