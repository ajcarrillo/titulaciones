<?php

namespace Titulaciones\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Titulaciones\Models\Alumno;
use Titulaciones\Models\Carrera;

class AlumnosImport implements ToModel, WithHeadingRow, WithChunkReading
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        //NumControl	Nombre	Apellidos	Sexo	Carrera	Reticula	PeriodoIngreso
        $primerApellido = trim($row['primer_apellido']);
        $segundoApellido = trim($row['segundo_apellido']);

        $nombre          = trim($row['nombre']);
        $apellidos       = "{$primerApellido} {$segundoApellido}";
        $carreraId       = trim($row['carrera']);
        $reticula        = trim($row['reticula']);
        $carrera         = $this->getCarrera($carreraId, $reticula);
        $periodo         = trim($row['periodoingreso']);
        $anio_ingreso    = substr($periodo, 0, 4);
        $periodo_ingreso = substr($periodo, 4, 1);

        return new Alumno([
            /*'numero_control'  => $row['numero_control'],
            'nombre_completo' => mb_strtoupper($row['nombre_completo'], 'utf-8'),
            'sexo'            => $row['sexo'],
            'carrera_id'      => $row['carrera_id'],*/
            'numero_control'  => trim($row['numcontrol']),
            'nombre'          => $nombre,
            'apellido'        => $apellidos,
            'nombre_apellido' => "{$nombre} {$apellidos}",
            'sexo'            => trim($row['sexo']),
            'anio_ingreso'    => $anio_ingreso,
            'periodo_ingreso' => $periodo_ingreso,
            'carrera_id'      => $carrera->id,
        ]);
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    protected function getCarrera($carreraId, $reticula)
    {
        return Carrera::query()
            ->where('sii_id', $carreraId)
            ->where('reticula', $reticula)
            ->first();
    }
}
