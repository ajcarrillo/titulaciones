<?php

namespace Titulaciones\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\ServiceProvider;
use Inertia\Inertia;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        //Fix laravel collective issue https://github.com/LaravelCollective/html/issues/531
        $this->app->make('form')->considerRequest(true);
        Inertia::setRootView('layouts.inertia');
        Inertia::version(function () {
            return md5_file(public_path('mix-manifest.json'));
        });
    }

    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }

        Inertia::share([
            'auth'  => function () {
                return [
                    'user' => Auth::user() ? [
                        'id'    => Auth::user()->id,
                        'name'  => Auth::user()->name,
                        'email' => Auth::user()->email,
                    ] : NULL,
                ];
            },
            'flash' => function () {
                return [
                    'success' => Session::get('success'),
                    'error'   => Session::get('error'),
                ];
            },
        ]);
    }
}
