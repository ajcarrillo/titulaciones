<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 12/03/21
 * Time: 6:49
 */

namespace Titulaciones\Http\Controllers;


use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Jenssegers\Date\Date;
use PhpOffice\PhpWord\Exception\CopyFileException;
use PhpOffice\PhpWord\TemplateProcessor;
use Titulaciones\Models\Nombramiento;

class GenerarAnexoTresController extends Controller
{
    public function update(Request $request, Nombramiento $nombramiento)
    {
        dd('xxxx');
        $nombramiento->update([
            'representante_division' => $request->input('representante_division'),
        ]);

        return ok([ 'msg' => 'El representante se actualizó correctamente' ]);
    }

    public function download(Nombramiento $nombramiento)
    {
        /*if (!isset($nombramiento->representante_division) || is_null($nombramiento->representante_division)) {
            return Redirect::route('titulaciones.index')->with('Error', 'La titulación no tiene representante asignado');
        }*/

        $nombramiento->loadMissing('alumno', 'alumno.carrera', 'opcion', 'horario', 'sinodales', 'sinodales.maestro');

        $nombre         = $nombramiento->alumno->nombre_apellido;
        $carrera        = $nombramiento->alumno->carrera->descripcion;
        $opcion         = $nombramiento->opcion->descripcion;
        $fecha          = new Date($nombramiento->fecha);
        $fecha          = $fecha->format('j \\d\\e F \\d\\e Y');
        $hora           = new Date($nombramiento->horario->hora);
        $presi          = $nombramiento->sinodales()->where('tipo', 'PRESIDENTE')->first();
        $secre          = $nombramiento->sinodales()->with('maestro')->where('tipo', 'SECRETARIO')->first();
        $vocal          = $nombramiento->sinodales()->with('maestro')->where('tipo', 'VOCAL')->first();
        $vocalSup       = $nombramiento->sinodales()->with('maestro')->where('tipo', 'VOCAL SUPLENTE')->first();
        $representante  = $nombramiento->representante_division;
        $presiEmail     = $presi->maestro->email;
        $presiEmailP    = "";
        $presiTel       = $presi->maestro->telefono;
        $secreEmail     = $secre->maestro->email;
        $secreEmailP    = "";
        $secreTel       = $secre->maestro->telefono;
        $vocalEmail     = $vocal->maestro->email;
        $vocalEmailP    = "";
        $vocalTel       = $vocal->maestro->telefono;
        $vocalSupEmail  = $vocalSup->maestro->email;
        $vocalSupEmailP = "";
        $vocalSupTel    = $vocalSup->maestro->telefono;

        try {
            $templateProcessor = new TemplateProcessor(storage_path('app/machotes/anexo_tres.docx'));

            $templateProcessor->setValue('nombre', $nombre);
            $templateProcessor->setValue('carrera', $carrera);
            $templateProcessor->setValue('opcion', $opcion);
            $templateProcessor->setValue('fecha', $fecha);
            $templateProcessor->setValue('hora', $hora->format('H:i'));
            $templateProcessor->setValue('presi', $presi->maestro->nombre_completo);
            $templateProcessor->setValue('secre', $secre->maestro->nombre_completo);
            $templateProcessor->setValue('vocal', $vocal->maestro->nombre_completo);
            $templateProcessor->setValue('vocalSup', $vocalSup->maestro->nombre_completo);
            $templateProcessor->setValue('representante', $representante);
            $templateProcessor->setValue('presiEmail', $presiEmail);
            $templateProcessor->setValue('presiEmailP', $presiEmailP);
            $templateProcessor->setValue('presiTel', $presiTel);
            $templateProcessor->setValue('secreEmail', $secreEmail);
            $templateProcessor->setValue('secreEmailP', $secreEmailP);
            $templateProcessor->setValue('secreTel', $secreTel);
            $templateProcessor->setValue('vocalEmail', $vocalEmail);
            $templateProcessor->setValue('vocalEmailP', $vocalEmailP);
            $templateProcessor->setValue('vocalTel', $vocalTel);
            $templateProcessor->setValue('vocalSupEmail', $vocalSupEmail);
            $templateProcessor->setValue('vocalSupEmailP', $vocalSupEmailP);
            $templateProcessor->setValue('vocalSupTel', $vocalSupTel);

            if (!\File::exists(storage_path('app/public/nombramientos/') . $nombramiento->id)) {
                \File::makeDirectory(storage_path('app/public/nombramientos/') . $nombramiento->id);
            }

            $templateProcessor->saveAs(storage_path('app/public/nombramientos/') . $nombramiento->id . '/anexo_tres.docx');

            return response()->download(storage_path('app/public/nombramientos/') . $nombramiento->id . '/anexo_tres.docx');
        } catch (CopyFileException | Exception $e) {
            \Log::info($e->getMessage());
            return Redirect::back()->with('error', 'No se pudo generar el archivo');
        }
    }
}
