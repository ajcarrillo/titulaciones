<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 21/09/20
 * Time: 2:10
 */

namespace Titulaciones\Http\Controllers\Catalogos;


use Titulaciones\Http\Controllers\Controller as BaseController;

abstract class CatalogoController extends BaseController
{
    protected $root = 'Titulaciones/Catalogos';
}
