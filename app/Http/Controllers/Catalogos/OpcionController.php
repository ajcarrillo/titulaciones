<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 21/09/20
 * Time: 2:09
 */

namespace Titulaciones\Http\Controllers\Catalogos;


use Inertia\Inertia;
use Titulaciones\Models\Opcion;

class OpcionController extends CatalogoController
{
    public function index()
    {
        return Inertia::render("{$this->root}/Opciones/Index", [
            "opciones" => $this->getOpciones(),
        ]);
    }

    protected function getOpciones()
    {
        return Opcion::query()
            ->with('modulos', 'generaciones')
            ->withCount('modulos', 'generaciones')
            ->orderBy('descripcion')
            ->get();
    }
}
