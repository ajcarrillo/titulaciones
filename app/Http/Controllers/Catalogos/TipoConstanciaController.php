<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 26/09/20
 * Time: 0:44
 */

namespace Titulaciones\Http\Controllers\Catalogos;


use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Redirect;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\TipoConstancia;

class TipoConstanciaController extends Controller
{
    protected $root = "Constancias/Catalogos/TipoConstancias";

    public function index()
    {
        return Inertia::render("{$this->root}/Index", [
            "tipos" => TipoConstancia::query()->get(),
        ]);
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            TipoConstancia::create([
                'descripcion' => $request->input('descripcion'),
                'file_name'   => $request->input('file_name'),
                'variables'   => $this->getVariablesArray($request->input('variables', '')),
            ]);

            $request->file->move(storage_path("app/machotes/constancias"), $request->input('file_name'));

            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }

        return Redirect::route('constancias.catalogos.tipos.contancias.index')->with('success', 'Tipo created.');
    }

    protected function getVariablesArray($variables)
    {
        if ($variables == '') {
            return [];
        }
        $userInput = collect(explode(',', $variables));
        $data      = $userInput->map(function ($el) {
            return Str::camel($el);
        });

        return $data;
    }
}
