<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-04
 * Time: 02:35
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Titulaciones\Models\Alumno;
use Titulaciones\Models\Carrera;
use Titulaciones\Models\Filters\AlumnoFilters;

class AlumnoController extends Controller
{
    protected $root = "Alumnos";

    public function index(Request $request, AlumnoFilters $filters)
    {
        if ($request->query('search', false)) {
            $data    = $request->only('search');
            $alumnos = Alumno::query()
                ->with('carrera')
                ->filterBy($filters, $data)
                ->get();
        }

        return Inertia::render("{$this->root}/Index", [
            "alumnos" => $alumnos ?? [],
        ]);
    }

    public function create()
    {
        $carreras = Carrera::query()
            ->orderBy('descripcion')
            ->get();

        return Inertia::render("{$this->root}/Create", [
            "carreras" => $carreras,
        ]);
    }

    public function store(Request $request)
    {
        $alumno                  = new Alumno($request->input());
        $alumno->nombre_apellido = "{$request->input('nombre')} {$request->input('apellido')}";

        $alumno->save();

        return Redirect::route('alumnos.index')->with('success', 'Alumno creado correctamente');
    }
}
