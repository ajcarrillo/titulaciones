<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-04
 * Time: 02:32
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class ImportarAlumnosController extends Controller
{
    public function import(Request $request)
    {
        $date = Carbon::now()->format('Y.m.d.H.i.s');

        $name      = $request->file('import_file')->getClientOriginalName();
        $filename  = mb_strtolower(pathinfo($name, PATHINFO_FILENAME), 'UTF-8');
        $extension = pathinfo($name, PATHINFO_EXTENSION);

        $request->file('import_file')->storeAs(
            'students', "{$date}_{$filename}.{$extension}", 'imports'
        );

        return redirect()
            ->route('alumnos.index')
            ->with([ 'file_name' => "php artisan import:alumnos {$date}_{$filename}.{$extension}" ]);
    }
}
