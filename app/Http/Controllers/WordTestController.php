<?php

namespace Titulaciones\Http\Controllers;

use Illuminate\Http\Request;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\TemplateProcessor;

class WordTestController extends Controller
{
    public function test()
    {
        // Creating the new document...
        $phpWord = new PhpWord();

        /* Note: any element you append to a document must reside inside of a Section. */

        // Adding an empty Section to the document...
        $section = $phpWord->addSection();

        $section->addText(
            '"Learn from yesterday, live for today, hope for tomorrow. '
            . 'The important thing is not to stop questioning." '
            . '(Albert Einstein)'
        );

        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        try {
            $objWriter->save(storage_path('app/public/helloWorld.docx'));
        } catch (\Exception $e) {
        }

        return response()->download(storage_path('app/public/helloWorld.docx'));
    }

    public function testReplace()
    {
        $templateProcessor = new TemplateProcessor(storage_path('app/public/helloWorld.docx'));
        $templateProcessor->setValue('myName', 'Andres Carrillo');
        $templateProcessor->saveAs(storage_path('app/public/AndresCarrillo.docx'));

        return response()->download(storage_path('app/public/AndresCarrillo.docx'));
    }
}
