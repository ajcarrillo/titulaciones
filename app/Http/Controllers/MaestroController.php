<?php

namespace Titulaciones\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Titulaciones\Models\Filters\MaestroFilters;
use Titulaciones\Models\Maestro;

class MaestroController extends Controller
{
    public function index(Request $request, MaestroFilters $filters)
    {
        $data = $request->only('search');
        $maestros = Maestro::query()
            ->filterBy($filters, $data)
            ->orderBy('nombre_completo')
            ->get();

        return Inertia::render('Maestros/Index', [
            'maestros' => $maestros,
            'filters' => $data,
        ]);
    }

    public function store(Request $request)
    {
        Maestro::create($request->input());

        return Redirect::back()->with('success', 'El maestro se creó correctamente');
    }

    public function update(Request $request, Maestro $maestro)
    {
        $maestro->update($request->input());

        return Redirect::back()->with('success', 'El maestro se actualizó correctamente');
    }
}
