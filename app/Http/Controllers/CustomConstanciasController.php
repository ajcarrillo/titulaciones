<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 14/02/21
 * Time: 0:27
 */

namespace Titulaciones\Http\Controllers;


use Jenssegers\Date\Date;
use PhpOffice\PhpWord\TemplateProcessor;
use Titulaciones\Models\Constancia;
use Titulaciones\Models\TipoConstancia;

class CustomConstanciasController
{
    public function download($constanciaId)
    {
        $constancia = Constancia::query()
            ->with('alumno', 'alumno.carrera')
            ->where('id', $constanciaId)
            ->first();

        $tipo = TipoConstancia::find($constancia->tipo_constancia_id);

        $templateProcessor = new TemplateProcessor(storage_path("app/machotes/constancias/{$tipo->file_name}"));
        Date::setLocale('es');

        $fecha      = new Date($constancia->fecha);
        $fecha      = $fecha->format('j \\d\\e F \\d\\e Y');
        $num_oficio = $constancia->numero_oficio;

        $templateProcessor->setValue('fecha', $fecha);
        $templateProcessor->setValue('num_oficio', $num_oficio);

        $variables = $tipo->variables;

        if (in_array("nombreAlumno", $variables)) {
            $nombreAlumno = $constancia->alumno->nombre_apellido;
            $templateProcessor->setValue('nombreAlumno', $nombreAlumno);
        }

        if (in_array("numeroControl", $variables)) {
            $numeroControl = $constancia->alumno->numero_control;
            $templateProcessor->setValue('numeroControl', $numeroControl);
        }

        if (in_array("carrera", $variables)) {
            $carrera = $constancia->alumno->carrera->descripcion;
            $templateProcessor->setValue('carrera', $carrera);
        }

        if ($constancia->asunto !== '' || $constancia->asunto !== NULL) {
            $asunto = $constancia->asunto;
            $templateProcessor->setValue('asunto', $asunto);
        }

        $variableNames = array_keys($constancia->configuration);

        foreach ($variableNames as $name) {
            $templateProcessor->setValue($name, $constancia->configuration[ $name ]);
        }

        if (in_array("numeroControl", $variables)) {
            $filename = $numeroControl;
        }else{
            $filename = date("YmdHi");
        }

        $templateProcessor->saveAs(storage_path("app/public/constancias/{$filename}_{$tipo->filename}.docx"));

        return response()->download(storage_path("app/public/constancias/{$filename}_{$tipo->filename}.docx"));
    }
}
