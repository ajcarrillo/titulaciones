<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 28/02/21
 * Time: 15:10
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Inertia\Inertia;
use Titulaciones\Models\Filters\NoAdeudoFilters;
use Titulaciones\Models\NoAdeudo;

class SolicitudNoAdeudoController extends Controller
{
    public function index(Request $request, NoAdeudoFilters $filters)
    {
        $data        = $request->only('search');
        $solicitudes = $this->getNoAdeudos($request, $filters);

        return Inertia::render('Solicitudes/NoAdeudos', [
            'solicitudes' => $solicitudes,
            'column'      => $this->getVariable(),
            'filters'     => $data,
        ]);
    }

    private function getNoAdeudos(Request $request, NoAdeudoFilters $filters)
    {
        $data  = $request->only('search');
        $query = NoAdeudo::query()
            ->with('alumno', 'alumno.carrera')
            ->filterBy($filters, $data);

        if (auth()->user()->hasRole('jefe_depto')) {
            $query->whereHas('alumno', function ($alumno) {
                $alumno->whereIn('carrera_id', auth()->user()->departamento->carreras()->pluck('id'));
            });
        }

        return $query->orderBy('created_at', 'desc')->get();
    }

    private function getVariable()
    {
        if (auth()->user()->hasRole('financieros')) {
            return 'adeuda_financieros';
        } else if (auth()->user()->hasRole('extraescolares')) {
            return 'adeuda_extraescolares';
        } else if (auth()->user()->hasRole('gestion_vinculacion')) {
            return 'adeuda_gestionvinculacion';
        } else if (auth()->user()->hasRole(['depto_academico', 'jefe_depto'])) {
            return 'adeuda_deptoacademico';
        } else if (auth()->user()->hasRole('biblioteca')) {
            return 'adeuda_biblioteca';
        } else if (auth()->user()->hasRole('division')) {
            return 'adeuda_divisionestudios';
        }
    }
}
