<?php

namespace Titulaciones\Http\Controllers;

use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use PhpOffice\PhpWord\TemplateProcessor;
use Titulaciones\Models\Nombramiento;

class NombramientoController extends Controller
{
    public function generarNombramiento(Request $request, Nombramiento $nombramiento)
    {
        Date::setLocale('es');
        $fecha   = new Date($nombramiento->fecha);
        $hora    = new Date($nombramiento->horario->hora);
        //$horario = Carbon::parse($hora)->subMinutes(30)->format('H:i');

        $nombramiento->load('alumno', 'alumno.carrera', 'alumno.carrera.jefeDepartamento', 'alumno.carrera.jefeDepartamento.responsable', 'horario', 'sinodales', 'sinodales.maestro', 'opcion', 'modulo');

        $opcion = $nombramiento->opcion;
        $modulo = $nombramiento->modulo;

        $templateProcessor = new TemplateProcessor(storage_path('app/machotes/machote_nombramiento.docx'));

        try {
            DB::transaction(function () use ($nombramiento, $opcion, $modulo, $hora, $fecha, $templateProcessor) {
                $presi    = $nombramiento->sinodales()->where('tipo', 'PRESIDENTE')->first();
                $secre    = $nombramiento->sinodales()->where('tipo', 'SECRETARIO')->first();
                $vocal    = $nombramiento->sinodales()->where('tipo', 'VOCAL')->first();
                $vocalSup = $nombramiento->sinodales()->where('tipo', 'VOCAL SUPLENTE')->first();

                $templateProcessor->setValue('num_oficio', $nombramiento->numero_oficio);
                $templateProcessor->setValue('fecha', Carbon::now()->format('d/m/Y'));
                $templateProcessor->setValue('presi_nombre', $presi->maestro->especialidad . ' ' . $presi->maestro->nombre_completo);
                $templateProcessor->setValue('presi_cedula', $presi->maestro->cedula_profesional);
                $templateProcessor->setValue('secre_nombre', $secre->maestro->especialidad . ' ' . $secre->maestro->nombre_completo);
                $templateProcessor->setValue('secre_cedula', $secre->maestro->cedula_profesional);
                $templateProcessor->setValue('vocal_nombre', $vocal->maestro->especialidad . ' ' . $vocal->maestro->nombre_completo);
                $templateProcessor->setValue('vocal_cedula', $vocal->maestro->cedula_profesional);
                $templateProcessor->setValue('vocal_sup_nombre', $vocalSup->maestro->especialidad . ' ' . $vocalSup->maestro->nombre_completo);
                $templateProcessor->setValue('vocal_sup_cedula', $vocalSup->maestro->cedula_profesional);
                $templateProcessor->setValue('opcion', $opcion->descripcion);
                $templateProcessor->setValue('modulo', optional($modulo)->descripcion);
                $templateProcessor->setValue('proyecto', htmlspecialchars($nombramiento->proyecto));
                $templateProcessor->setValue('alumno', $nombramiento->alumno->nombre_apellido);
                $templateProcessor->setValue('num_control', $nombramiento->alumno->numero_control);
                $templateProcessor->setValue('carrera', $nombramiento->alumno->carrera->descripcion);
                $templateProcessor->setValue('fecha_prog', strtoupper($fecha->format('l j F \\d\\e Y')));
                $templateProcessor->setValue('horario', $hora->format('H:i'));
                $templateProcessor->setValue('jefe_depto', $nombramiento->alumno->carrera->jefeDepartamento->responsable->name);
                $templateProcessor->setValue('jefatura', $nombramiento->alumno->carrera->jefeDepartamento->jefatura);
                //$templateProcessor->setValue('horas', $horario);

                $seccion = substr($nombramiento->alumno->carrera->jefeDepartamento->jefatura, 18, strlen($nombramiento->alumno->carrera->jefeDepartamento->jefatura));

                $templateProcessor->setValue('seccion', $seccion);

                if (!\File::exists(storage_path('app/public/nombramientos/') . $nombramiento->id)) {
                    \File::makeDirectory(storage_path('app/public/nombramientos/') . $nombramiento->id);
                }

                $templateProcessor->saveAs(storage_path('app/public/nombramientos/') . $nombramiento->id . '/nombramiento.docx');
            });

            return response()->download(storage_path('app/public/nombramientos/') . $nombramiento->id . '/nombramiento.docx');
        } catch (\Exception $e) {
            abort(500, $e->getMessage());
        } catch (\Throwable $e) {
            abort(500, $e->getMessage());
        }
    }
}
