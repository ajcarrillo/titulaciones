<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/09/20
 * Time: 20:20
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Inertia\Inertia;
use Jenssegers\Date\Date;
use PhpOffice\PhpWord\TemplateProcessor;
use Redirect;
use Titulaciones\Models\Constancia;
use Titulaciones\Models\Filters\ConstanciaFilters;
use Titulaciones\Models\TipoConstancia;

class ConstanciaController extends Controller
{
    protected $root = 'Constancias';

    public function index(Request $request, ConstanciaFilters $filters)
    {
        $data        = $request->only('search', 'tipo');
        $constancias = $this->getConstancias($filters, $data);
        $tipos       = $this->getTipos();

        return Inertia::render("{$this->root}/Index", [
            'constancias' => $constancias,
            'tipos'       => $tipos,
        ]);
    }

    private function getConstancias(ConstanciaFilters $filters, array $data)
    {
        $constancias = Constancia::query()
            ->with('alumno', 'tipo', 'alumno.carrera')
            ->filterBy($filters, $data)
            ->get();

        return $constancias;
    }

    public function create()
    {
        $tipos = $this->getTipos();

        return Inertia::render("{$this->root}/Create", [
            'tipos' => $tipos,
        ]);
    }

    private function getTipos()
    {
        $tipos = TipoConstancia::query()->orderBy('descripcion')->get();

        return $tipos;
    }

    public function store(Request $request)
    {
        $constancia                = new Constancia($request->input());
        $constancia->configuration = $request->input('configuration');

        $constancia->save();

        return Redirect::route('constancias.index', [
            'search' => $constancia->alumno->numero_control,
            'tipo'   => $constancia->tipo_constancia_id,
        ])->with('success', 'Constancia creada');
    }

    public function download($id)
    {
        $constancia = Constancia::query()
            ->with('alumno', 'alumno.carrera')
            ->where('id', $id)
            ->first();

        $tipo = TipoConstancia::find($constancia->tipo_constancia_id);

        $templateProcessor = new TemplateProcessor(storage_path("app/machotes/constancias/{$tipo->file_name}"));
        Date::setLocale('es');

        $fecha            = new Date($constancia->created_at);
        $fecha            = $fecha->format('j \\d\\e F \\d\\e Y');
        $num_oficio       = $constancia->numero_oficio;
        $nombre_apellidos = $constancia->alumno->nombre_apellido;
        $num_control      = $constancia->alumno->numero_control;
        $carrera          = $constancia->alumno->carrera->descripcion;

        $templateProcessor->setValue('fecha', $fecha);
        $templateProcessor->setValue('num_oficio', $num_oficio);
        $templateProcessor->setValue('nombre_apellidos', $nombre_apellidos);
        $templateProcessor->setValue('num_control', $num_control);
        $templateProcessor->setValue('carrera', $carrera);

        $templateProcessor->saveAs(storage_path("app/public/constancias/{$num_control}_constancia_no_inconveniencia_ingles.docx"));

        return response()->download(storage_path("app/public/constancias/{$num_control}_constancia_no_inconveniencia_ingles.docx"));
    }
}
