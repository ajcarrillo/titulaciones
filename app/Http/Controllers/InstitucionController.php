<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 13/09/20
 * Time: 20:16
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Inertia\Inertia;
use Redirect;
use Titulaciones\Models\Institucion;
use URL;

class InstitucionController extends Controller
{
    protected $root = 'Instituciones';

    public function index()
    {
        return Inertia::render("{$this->root}/Index", [
            'instituciones' => Institucion::all()->map(function ($el) {
                return [
                    'descripcion'   => $el->descripcion,
                    'director'      => $el->director,
                    'jefe_division' => $el->jefe_division,
                    'edit_url'      => URL::route('instituciones.edit', [ 'institucion' => $el->id ]),
                ];
            }),
            'create_url'    => URL::route('instituciones.create'),
        ]);
    }

    public function create()
    {
        return Inertia::render("{$this->root}/Create", []);
    }

    public function store(Request $request)
    {
        $new = new Institucion($request->input());

        $new->save();

        return Redirect::route('instituciones.index');
    }

    public function edit(Institucion $institucion)
    {
        return Inertia::render("{$this->root}/Edit", [
            'institucion' => $institucion,
        ]);
    }

    public function update(Request $request, Institucion $institucion)
    {
        $institucion->update($request->input());

        return Redirect::back()->with('success', 'Institucion updated.');
    }
}
