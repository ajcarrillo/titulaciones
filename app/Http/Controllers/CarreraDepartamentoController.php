<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-03
 * Time: 22:49
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Titulaciones\Models\Carrera;
use Titulaciones\Models\JefeDepartamento;

class CarreraDepartamentoController extends Controller
{
    public function index()
    {
        $carreras      = $this->getCarreras();
        $departamentos = $this->getDepartamentos();

        return view('administracion.carrera_departamento.index', compact('carreras', 'departamentos'));
    }

    protected function getCarreras()
    {
        return Carrera::orderBy('descripcion')
            ->get([ 'id', 'descripcion' ]);
    }

    protected function getDepartamentos()
    {
        return JefeDepartamento::orderBy('jefatura')
            ->get([ 'id', 'jefatura' ]);
    }

    public function store(Request $request)
    {
        $carreras = Carrera::whereIn('id', $request->input('carrera_id'))->get();

        foreach ($carreras as $carrera) {
            $carrera->update([
                'jefe_departamento_id' => $request->input('jefe_departamento_id'),
            ]);
        }

        flash('Las carreras se actualizaron correctamente')->success();

        return back();
    }
}