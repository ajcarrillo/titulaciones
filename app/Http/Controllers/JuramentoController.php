<?php

namespace Titulaciones\Http\Controllers;

use Jenssegers\Date\Date;
use PhpOffice\PhpWord\TemplateProcessor;
use Titulaciones\Models\Nombramiento;

class JuramentoController extends Controller
{
    public function generarJuramento(Nombramiento $nombramiento)
    {
        Date::setLocale('es');
        $nombramiento->load('alumno.carrera', 'alumno', 'horario');

        $fecha = new Date($nombramiento->fecha);
        $hora  = new Date($nombramiento->horario->hora);

        $templateProcessor = new TemplateProcessor(storage_path('app/machotes/machote_juramento.docx'));

        $templateProcessor->setValue('dia', strtoupper($fecha->format('j')));
        $templateProcessor->setValue('mes', strtoupper($fecha->format('F')));
        $templateProcessor->setValue('year', $fecha->format('Y'));
        $templateProcessor->setValue('titulo', $nombramiento->alumno->carrera->grado);
        $templateProcessor->setValue('alumno', $nombramiento->alumno->nombre_apellido);

        if ( ! \File::exists(storage_path('app/public/nombramientos/') . $nombramiento->id)) {
            \File::makeDirectory(storage_path('app/public/nombramientos/') . $nombramiento->id);
        }

        $templateProcessor->saveAs(storage_path('app/public/nombramientos/') . $nombramiento->id . '/juramento.docx');

        return response()->download(storage_path('app/public/nombramientos/') . $nombramiento->id . '/juramento.docx');

    }
}
