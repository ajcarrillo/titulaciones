<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 28/02/21
 * Time: 12:15
 */

namespace Titulaciones\Http\Controllers\ServiciosEscolares;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Titulaciones\Events\UpdateNoAdeudo;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\NoAdeudo;

class UpdateNoAdeudoController extends Controller
{
    public function update(Request $request, NoAdeudo $adeudo)
    {
        $field = $request->input('column');
        $value = $request->input('value');

        $adeudo->update([
            $field => $value
        ]);

        event(new UpdateNoAdeudo($adeudo));

        return Redirect::back()->with('success', 'La solicitud se actualizó correctamente');
    }
}
