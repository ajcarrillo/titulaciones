<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/02/21
 * Time: 0:27
 */

namespace Titulaciones\Http\Controllers\ServiciosEscolares;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\Filters\NoAdeudoFilters;
use Titulaciones\Models\NoAdeudo;

class NoAudeudosController extends Controller
{
    public function index(Request $request, NoAdeudoFilters $filters)
    {
        $data        = $request->only('search');
        $solicitudes = NoAdeudo::query()
            ->with('alumno', 'alumno.carrera')
            ->filterBy($filters, $data)
            ->orderBy('created_at', 'desc')
            ->get();

        return Inertia::render('ServiciosEscolares/NoAdeudos/Index', [
            'solicitudes' => $solicitudes,
            'filters'     => $data,
        ]);
    }

    public function store(Request $request)
    {
        $solicitud            = new NoAdeudo();
        $solicitud->alumno_id = $request->input('alumno_id');
        $solicitud->estatus   = 'PENDIENTE';
        $solicitud->save();

        return Redirect::back()->with('success', 'La solicitud se creó correctamente');
    }
}
