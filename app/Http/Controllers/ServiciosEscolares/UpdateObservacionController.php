<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 28/02/21
 * Time: 17:53
 */

namespace Titulaciones\Http\Controllers\ServiciosEscolares;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\NoAdeudo;

class UpdateObservacionController extends Controller
{
    public function update(Request $request, NoAdeudo $adeudo)
    {
        $field = $request->input('column');
        $value = $request->input('value');

        $adeudo->update([
            $field => $value
        ]);

        return Redirect::back()->with('success', 'La solicitud se actualizó correctamente');
    }
}
