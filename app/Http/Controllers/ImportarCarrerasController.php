<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-04
 * Time: 00:55
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Titulaciones\Imports\CarrerasImport;

class ImportarCarrerasController extends Controller
{
    public function import(Request $request)
    {
        $path = $request->file('import_file')->getRealPath();

        Excel::import(new CarrerasImport, $path, NULL, \Maatwebsite\Excel\Excel::XLS);

        flash('El archivo se cargó correctamente')->success();

        return back();
    }
}
