<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 03/09/20
 * Time: 16:30
 */

namespace Titulaciones\Http\Controllers;


use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{
    public function edit()
    {
        return view('users.reset_password');
    }

    public function update(Request $request)
    {
        $request->validate([
            'password' => 'required|min:6|max:10',
        ]);

        $user = Auth::user();

        $user->password = Hash::make($request->input('password'));

        $user->setRememberToken(Str::random(60));

        $user->update();

        flash('Tu contraseña se actualizó correctamente')->success();

        return back();
    }
}
