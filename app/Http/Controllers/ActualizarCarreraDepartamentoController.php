<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-04
 * Time: 13:37
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Titulaciones\Models\Carrera;

class ActualizarCarreraDepartamentoController extends Controller
{
    public function update(Request $request)
    {
        $carrera = Carrera::query()
            ->where('sii_id', $request->input('sii_id'));

        $carrera->update([
            'jefe_departamento_id' => $request->input('jefe_departamento_id'),
        ]);

        flash('La carrera se actualizó correctamente')->success();

        return back();
    }
}
