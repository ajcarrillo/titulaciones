<?php

namespace Titulaciones\Http\Controllers;

use Illuminate\Http\Request;
use Titulaciones\Models\JefeDepartamento;
use Titulaciones\User;

class JefeDepartamentoController extends Controller
{
    public function index()
    {
        $usuarios      = User::orderBy('name')->pluck('name', 'id');
        $departamentos = JefeDepartamento::with('responsable')
            ->orderBy('jefatura')
            ->get();

        return view('jefes_departamento.index', compact('usuarios', 'departamentos'));
    }

    public function store(Request $request)
    {
        JefeDepartamento::create($request->input());

        flash('El departamento se creo correctamente')->success();

        return redirect()->back();
    }

    public function update(Request $request, JefeDepartamento $departamento)
    {
        $departamento->update($request->input());

        flash('El departamento se actualizó correctamente')->success();

        return redirect()->back();
    }
}
