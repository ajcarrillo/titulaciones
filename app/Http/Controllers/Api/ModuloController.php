<?php

namespace Titulaciones\Http\Controllers\Api;

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\Models\Carrera;
use Titulaciones\Models\Opcion;

class ModuloController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $opc     = \Request::query('opcion');
        $carrera = \Request::query('carrera');

        $opcion = Opcion::find($opc);

        if ($opcion->clave == 'VI') {
            $modulos = Carrera::with([ 'modulos' => function ($query) {
                $query->selectRaw('modulos_titulacion.id as value, modulos_titulacion.descripcion as text');
            } ])->where('id', $carrera)->first();
        } else {
            $modulos = Opcion::with([ 'modulos' => function ($query) {
                $query->selectRaw('modulos_titulacion.id as value, modulos_titulacion.descripcion as text');
            } ])->where('id', $opc)->first();
        }


        return $this->respondWithArray(compact('opcion', 'modulos'));
    }
}
