<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 27/09/20
 * Time: 22:35
 */

namespace Titulaciones\Http\Controllers\Api\v1;


use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\Alumno;
use Titulaciones\Models\Filters\AlumnoFilters;

class AlumnoController extends Controller
{
    public function index(Request $request, AlumnoFilters $filters)
    {
        $data = $request->only('search');

        $alumnos = Alumno::query()
            ->with('carrera')
            ->filterBy($filters, $data)
            ->take(10)
            ->get();

        return ok(compact('alumnos'));
    }
}
