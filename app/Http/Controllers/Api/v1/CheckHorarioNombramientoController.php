<?php


namespace Titulaciones\Http\Controllers\Api\v1;


use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\Nombramiento;

class CheckHorarioNombramientoController extends Controller
{
    public function check(Request $request)
    {
        $fecha          = $request->query('fecha');
        $horarioId      = $request->query('horario_id');
        $isEdit         = $request->query('isEdit', false);
        $nombramientoId = $request->query('nombramientoId', false);

        $query = Nombramiento::query()
            ->where('fecha', $fecha)
            ->where('horario_id', $horarioId)
            ->when($isEdit, function ($q) use ($nombramientoId) {
                return $q->where('id', '!=', $nombramientoId);
            });

        $exists = $query->exists();

        return ok(compact('exists'));
    }
}