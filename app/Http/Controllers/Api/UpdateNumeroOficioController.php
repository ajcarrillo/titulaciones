<?php

namespace Titulaciones\Http\Controllers\Api;

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\Models\Nombramiento;

class UpdateNumeroOficioController extends Controller
{
    use ResponseTrait;

    public function show(Nombramiento $nombramiento)
    {
        $numeroOficio = $nombramiento->numero_oficio;

        return $this->respondWithArray(compact('numeroOficio'));
    }

    public function update(Request $request, Nombramiento $nombramiento)
    {
        $nombramiento->update([ 'numero_oficio' => $request->input('numero_oficio'), 'estatus' => 'C' ]);

        return $this->respondWithArray([ 'is_valid' => true ]);
    }
}
