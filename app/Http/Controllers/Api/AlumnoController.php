<?php

namespace Titulaciones\Http\Controllers\Api;

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\Models\Alumno;

class AlumnoController extends Controller
{
    use ResponseTrait;

    public function index(Request $request)
    {
        $alumno = Alumno::with('carrera', 'nombramiento')
            ->where('numero_control', $request->query('numero_control'))
            ->first();

        if (is_null($alumno)) {
            return $this->sendNotFoundResponse();
        }

        return $this->respondWithArray(compact('alumno'));
    }
}
