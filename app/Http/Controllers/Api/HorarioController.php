<?php

namespace Titulaciones\Http\Controllers\Api;

use Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\Models\Horario;

class HorarioController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $fecha = Request::query('fecha');

        $horarios = Horario::selectRaw('id as value, SUBSTR(hora,1,5) as text')
            ->get();

        return $this->respondWithArray(compact('horarios'));
    }
}
