<?php

namespace Titulaciones\Http\Controllers\Api;

use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\User;

class ResetPasswordController extends Controller
{
    use ResponseTrait;

    public function update(Request $request, User $usuario)
    {
        try {
            $this->resetPassword($usuario, $request->input('new_password'));

            return $this->respondWithArray(compact('usuario'));
        } catch (\Throwable $e) {
            return $this->setException($e)->respondeWithErrorsException();
        }
    }

    protected function resetPassword($user, $password)
    {
        $user->password = Hash::make($password);

        $user->setRememberToken(Str::random(60));

        $user->update();
    }
}
