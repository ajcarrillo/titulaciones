<?php

namespace Titulaciones\Http\Controllers\Api;

use DB;
use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\Models\Carrera;
use Titulaciones\Models\Horario;
use Titulaciones\Models\Nombramiento;
use Titulaciones\Models\NumeroOficio;
use Titulaciones\Models\Opcion;

/*
 * DEPRECATED IN FAVOR OF app/Http/Controllers/TitulacionController.php
 * */
class NombramientoController extends Controller
{
    use ResponseTrait;

    protected $result = [
        'is_valid' => false,
        'errors'   => NULL,
        'msg'      => NULL,
    ];

    public function getData()
    {
        $carreras = Carrera::selectRaw('id as value, descripcion as text')->orderBy('descripcion')->get();
        $opciones = Opcion::selectRaw('id as value, concat_ws(" ",clave,descripcion) as text')->orderBy('clave')->get();
        $horas    = Horario::selectRaw('id as value, hora as text')->orderBy('hora')->get();

        return response()->json(compact('carreras', 'opciones', 'horas'));
    }

    public function index(Request $request)
    {
        $query = Nombramiento::with('sinodales', 'sinodales.maestro', 'memo', 'alumno', 'alumno.carrera', 'opcion', 'modulo', 'horario');

        if (currentAPIUser()->hasRole([ 'division', 'titulacion' ])) {
            $nombramientos = $query->get();
        } elseif (currentAPIUser()->hasRole('jefe_depto')) {
            $nombramientos = $query->whereHas('alumno', function ($alumno) {
                $alumno->whereIn('carrera_id', currentAPIUser()->departamento->carreras()->pluck('id'));
            })->whereIn('estatus', [ 'E', 'C' ])->get();
        }

        return $this->respondWithArray(compact('nombramientos'));
    }

    public function store(Request $request)
    {

        try {
            DB::beginTransaction();

            $data   = $request->post('nombramiento');
            $oficio = NumeroOficio::getNumeroOficio();
            $oficio->update([ 'activo' => 0 ]);

            $nombramiento                 = new Nombramiento($data);
            $nombramiento->estatus        = 'P';
            $nombramiento->numero_memo_id = $oficio->id;
            $nombramiento->save();

            DB::commit();

            return $this->respondWithArray(compact('nombramiento'));
        } catch (\Exception $e) {
            DB::rollBack();

            return unprocessable_entity([ $e->getMessage() ]);
        }
    }

    public function storeSinodales(Request $request, Nombramiento $nombramiento)
    {
        try {
            DB::transaction(function () use ($request, $nombramiento) {
                $presi         = $request->post('presidente');
                $secretario    = $request->post('secretario');
                $vocal         = $request->post('vocal');
                $vocalSuplente = $request->post('vocalSuplente');

                if ( ! is_null($presi)) {
                    $nombramiento->sinodales()->updateOrCreate(
                        [ 'tipo' => 'PRESIDENTE' ], [ 'maestro_id' => $presi ]
                    );
                }

                if ( ! is_null($secretario)) {
                    $nombramiento->sinodales()->updateOrCreate(
                        [ 'tipo' => 'SECRETARIO' ], [ 'maestro_id' => $secretario ]
                    );
                }

                if ( ! is_null($vocal)) {
                    $nombramiento->sinodales()->updateOrCreate(
                        [ 'tipo' => 'VOCAL' ], [ 'maestro_id' => $vocal ]
                    );
                }

                if ( ! is_null($vocalSuplente)) {
                    $nombramiento->sinodales()->updateOrCreate(
                        [ 'tipo' => 'VOCAL SUPLENTE' ], [ 'maestro_id' => $vocalSuplente ]
                    );
                }
            });
            $this->result['is_valid'] = true;
        } catch (\Exception $e) {
            $this->result['msg']    = $e->getMessage();
            $this->result['errors'] = $e->getTraceAsString();
        } catch (\Throwable $e) {
            $this->result['msg']    = $e->getMessage();
            $this->result['errors'] = $e->getTraceAsString();
        }

        return response()->json($this->result);
    }

    public function getSinodales(Nombramiento $nombramiento)
    {
        $nombramiento->load('sinodales', 'sinodales.maestro');

        return ok(compact('nombramiento'));
    }
}
