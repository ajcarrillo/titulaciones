<?php

namespace Titulaciones\Http\Controllers\Api;

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\Role;

class RolController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $roles = Role::get([ 'id', 'descripcion' ]);

        return $this->respondWithArray(compact('roles'));
    }
}
