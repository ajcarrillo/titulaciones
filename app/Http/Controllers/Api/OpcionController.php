<?php

namespace Titulaciones\Http\Controllers\Api;

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\Models\Generacion;
use Titulaciones\Models\Opcion;

class OpcionController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $year = \Request::query('year');

        $query = Generacion::with([ 'opciones' => function ($query) {
            $query->selectRaw('opciones_titulacion.id, concat_ws(" ", opciones_titulacion.clave, opciones_titulacion.descripcion) as text')
                ->orderBy('clave');
        } ]);

        $generacion = $query->whereRaw($year . " BETWEEN inicio and fin")
            ->first();

        $generacion->opciones()->get();

        return $this->respondWithArray(compact('generacion'));
    }
}
