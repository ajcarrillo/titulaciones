<?php

namespace Titulaciones\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\Models\Nombramiento;

class TitulacionController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $query = Nombramiento::with('alumno', 'alumno.carrera', 'modulo', 'opcion', 'horario')
            ->where('estatus', 'C')
            ->orderBy('fecha', 'ASC')
            ->orderBy('horario_id', 'ASC');

        $del = \Request::query('del',Carbon::now()->format('Y-m-d'));
        $al = \Request::query('al',Carbon::now()->format('Y-m-d'));

        $query->whereBetween('fecha', [$del, $al]);

        $titulaciones = $query->get();

        return $this->respondWithArray(compact('titulaciones'));
    }
}
