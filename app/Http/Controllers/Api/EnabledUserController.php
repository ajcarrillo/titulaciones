<?php

namespace Titulaciones\Http\Controllers\Api;

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\User;

class EnabledUserController extends Controller
{
    use ResponseTrait;

    public function update(User $usuario)
    {
        $usuario->enabled();

        return $this->respondWithArray(compact('usuario'));
    }

    public function destroy(User $usuario)
    {
        $usuario->disabled();

        return $this->respondWithArray(compact('usuario'));
    }
}
