<?php

namespace Titulaciones\Http\Controllers\Api;

use DB;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Http\Controllers\ResponseTrait;
use Titulaciones\User;

class UserController extends Controller
{
    use ResponseTrait;

    public function index()
    {
        $users = User::with('roles')->get();

        return response()->json(compact('users'), Response::HTTP_OK);
    }

    public function store(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $user = $this->create($request->input('usuario'));
                $user->roles()->sync($request->input('usuario')['selected_roles']);
            });

            return $this->setStatusCode(Response::HTTP_CREATED)->respondWithArray(compact('user'));
        } catch (\Throwable $e) {
            $errors = [ 'error' => $e->getMessage(), 'trace' => $e->getTraceAsString() ];

            return $this->setException($e)->respondWithArray(compact('errors'));
        }
    }

    public function show(User $usuario)
    {
        return $this->respondWithArray(compact('usuario'));
    }

    public function update(Request $request, User $usuario)
    {
        try {
            DB::transaction(function () use ($request, $usuario) {
                $usuario->update($request->input('usuario'));
                $usuario->roles()->sync($request->input('usuario')['selected_roles']);
            });

            return $this->setStatusCode(Response::HTTP_OK)->respondWithArray(compact('user'));
        } catch (\Throwable $e) {
            $errors = [ 'error' => $e->getMessage(), 'trace' => $e->getTraceAsString() ];

            return $this->setException($e)->respondWithArray(compact('errors'));
        }


    }

    protected function create(array $data)
    {
        return User::create([
            'name'      => $data['name'],
            'email'     => $data['email'],
            'username'  => $data['username'],
            'active'    => true,
            'api_token' => str_random(50),
            'password'  => Hash::make($data['password']),
        ]);
    }
}
