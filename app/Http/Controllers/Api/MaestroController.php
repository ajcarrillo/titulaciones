<?php

namespace Titulaciones\Http\Controllers\Api;

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\Maestro;

class MaestroController extends Controller
{
    public function searchMaestros(Request $request)
    {
        $maestros = Maestro::orderBy('nombre_completo')
            ->selectRaw('id as value, nombre_completo as label')
            ->where('nombre_completo', 'LIKE', '%'.$request->query('term').'%')
            ->get();

        return response()->json(compact('maestros'));
    }
}
