<?php

namespace Titulaciones\Http\Controllers\Api;

use Illuminate\Http\Request;
use Titulaciones\Http\Controllers\Controller;
use Titulaciones\Models\Carrera;

class CarreraController extends Controller
{
    public function carreras()
    {
        $carreras =  Carrera::selectRaw('id as value, descripcion as text')->orderBy('descripcion')->get();

        return response()->json(compact('carreras'));
    }
}
