<?php

namespace Titulaciones\Http\Controllers;

use Titulaciones\Models\Maestro;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $maestros = $this->getMaestros();

        return view('home', [
            'maestros' => $maestros,
        ]);
    }

    public function serviciosEscolares()
    {
        return view('servicios_escolares');
    }

    private function getMaestros()
    {
        return Maestro::query()
            ->orderBy('nombre_completo')
            ->selectRaw('id as value, nombre_completo as label')
            ->get();
    }
}
