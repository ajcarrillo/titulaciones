<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 2019-03-04
 * Time: 00:20
 */

namespace Titulaciones\Http\Controllers;


use Illuminate\Http\Request;
use Titulaciones\Models\Carrera;
use Titulaciones\Models\JefeDepartamento;

class CarrerasController extends Controller
{

    public function index(Request $request)
    {
        $carreras = Carrera::with('jefeDepartamento', 'jefeDepartamento.responsable')
            ->where('nivel_escolar', '!=', 'P')
            ->where('descripcion', 'like', "%{$request->input('search')}%")
            ->orderBy('descripcion')
            ->groupBy('sii_id')
            ->get();

        $departamentos = JefeDepartamento::orderBy('jefatura')->get();

        return view('administracion.carreras.index', compact('carreras', 'departamentos'));
    }

}
