<?php

namespace Titulaciones\Http\Controllers;


use Carbon\Carbon;
use DB;
use Exception;
use File;
use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use Log;
use PhpOffice\PhpWord\TemplateProcessor;
use Titulaciones\Models\Nombramiento;

class MemoSinodalesController extends Controller
{
    public function generarMemoSinodales(Request $request, Nombramiento $nombramiento)
    {
        Date::setLocale('es');
        $fecha = new Date($nombramiento->fecha);
        $hora  = new Date($nombramiento->horario->hora);

        $nombramiento->load('alumno.carrera', 'alumno.carrera.jefeDepartamento', 'alumno.carrera.jefeDepartamento.responsable', 'alumno', 'horario', 'opcion', 'modulo', 'memo');

        if (is_null($nombramiento->alumno->carrera->jefeDepartamento)) {
            if ($request->ajax()) {
                return response()->json([ 'msg' => 'La carrera no tiene jefe de departamento asignado' ], 422);
            }
            abort(422, 'La carrera no tiene jefe de departamento asignado');
        }

        $opcion  = $nombramiento->opcion;
        $modulo  = $nombramiento->modulo;
        $jefatzo = $nombramiento->alumno->carrera->jefeDepartamento;

        try {
            $templateProcessor = new TemplateProcessor(storage_path('app/machotes/machote_memos_sinodales.docx'));
            DB::transaction(function () use ($nombramiento, $opcion, $modulo, $jefatzo, $hora, $fecha, $templateProcessor) {

                if ($nombramiento->estatus == 'P') {
                    $nombramiento->update([ 'estatus' => 'E' ]);
                }

                $nombreCarreraOriginal = $nombramiento->alumno->carrera->descripcion;
                $nombreCarrera         = preg_replace("/\S*ABIERTO\S*/i", "", trim($nombreCarreraOriginal));

                $templateProcessor->setValue('jefeDepartamento', $jefatzo->responsable->name);
                $templateProcessor->setValue('departamento', $jefatzo->jefatura);
                $templateProcessor->setValue('alumnoNombreCompleto', $nombramiento->alumno->nombre_apellido);
                $templateProcessor->setValue('carrera', $nombreCarrera);
                $templateProcessor->setValue('opcion', $opcion->descripcion);
                $templateProcessor->setValue('modulo', optional($modulo)->descripcion);
                $templateProcessor->setValue('horario', $hora->format('H:i'));
                $templateProcessor->setValue('oficio', $nombramiento->memo->numero_oficio);
                $templateProcessor->setValue('fecha_ofi', Carbon::now()->format('d/m/Y'));
                $templateProcessor->setValue('fecha_prog', strtoupper($fecha->format('l j F \\d\\e Y')));
                $templateProcessor->setValue('num_control', $nombramiento->alumno->numero_control);
                $templateProcessor->setValue('proyecto', htmlspecialchars($nombramiento->proyecto));

                if (!File::exists(storage_path('app/public/nombramientos/') . $nombramiento->id)) {
                    File::makeDirectory(storage_path('app/public/nombramientos/') . $nombramiento->id);
                }

                $templateProcessor->saveAs(storage_path('app/public/nombramientos/') . $nombramiento->id . '/memo_sinodales.docx');
            });

            return response()->download(storage_path('app/public/nombramientos/') . $nombramiento->id . '/memo_sinodales.docx');
        } catch (Exception $e) {
            Log::info($e->getTraceAsString());
            Log::info($e->getMessage());
            abort(500, $e->getMessage());
        }
    }
}
