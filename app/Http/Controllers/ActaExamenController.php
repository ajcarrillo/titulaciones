<?php

namespace Titulaciones\Http\Controllers;

use Jenssegers\Date\Date;
use PhpOffice\PhpWord\TemplateProcessor;
use Titulaciones\Models\Nombramiento;

class ActaExamenController extends Controller
{
    public function generarActaExamen(Nombramiento $nombramiento)
    {
        Date::setLocale('es');
        $nombramiento->load('alumno.carrera', 'alumno');

        $fecha = new Date($nombramiento->fecha);
        $secre = $nombramiento->sinodales()->where('tipo', 'SECRETARIO')->first();

        $secretario = $secre->maestro->especialidad . ' ' . $secre->maestro->nombre_completo;
        $cedula     = $secre->maestro->cedula_profesional;

        $templateProcessor = new TemplateProcessor(storage_path('app/machotes/machote_acta_examen.docx'));

        $templateProcessor->setValue('dia', strtoupper($fecha->format('j')));
        $templateProcessor->setValue('mes', strtoupper($fecha->format('F')). ' de ' . $fecha->format('Y') );

        $templateProcessor->setValue('alumno', strtoupper($nombramiento->alumno->nombre_apellido));
        $templateProcessor->setValue('carrera', strtoupper($nombramiento->alumno->carrera->descripcion));
        $templateProcessor->setValue('secretario', strtoupper($secretario));
        $templateProcessor->setValue('cedula', strtoupper($cedula));

        if ( ! \File::exists(storage_path('app/public/nombramientos/') . $nombramiento->id)) {
            \File::makeDirectory(storage_path('app/public/nombramientos/') . $nombramiento->id);
        }

        $templateProcessor->saveAs(storage_path('app/public/nombramientos/') . $nombramiento->id . '/acta.docx');

        return response()->download(storage_path('app/public/nombramientos/') . $nombramiento->id . '/acta.docx');
    }
}
