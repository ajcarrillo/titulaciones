<?php

namespace Titulaciones\Http\Controllers;

use Illuminate\Http\Request;
use Jenssegers\Date\Date;
use PhpOffice\PhpWord\TemplateProcessor;
use Titulaciones\Models\Nombramiento;

class SolicitudIntervencionSustentanteController extends Controller
{
    public function __invoke(Nombramiento $nombramiento)
    {
        Date::setLocale('es');
        $nombramiento->load('alumno.carrera', 'alumno', 'horario');

        $fechaRealizacion = new Date($nombramiento->fecha);
        $today            = Date::now();

        $horaPrueba      = new Date($nombramiento->horario->hora);
        $horaRealizacion = new Date($nombramiento->horario->hora);

        $fecha_oficio      = $today->format('j \\d\\e F \\d\\e Y');
        $fecha_limite      = $today->add('1 day')->format('j \\d\\e F \\d\\e Y');
        $hora_prueba       = $horaPrueba->sub('30 min')->format('H:i');
        $hora_realizacion  = $horaRealizacion->format('H:i');
        $fecha_realizacion = $fechaRealizacion->format('j \\d\\e F \\d\\e Y');
        $opcion_titulacion = "{$nombramiento->opcion->descripcion}/{$nombramiento->modulo->descripcion}";
        $carrera           = $nombramiento->alumno->carrera->descripcion;
        $num_control       = $nombramiento->alumno->numero_control;
        $alumno            = $nombramiento->alumno->nombre_apellido;

        $templateProcessor = new TemplateProcessor(storage_path('app/machotes/machote_solicitud_intervencion_sustentante.docx'));

        $templateProcessor->setValue('fecha_limite', $fecha_limite);
        $templateProcessor->setValue('fecha_oficio', $fecha_oficio);
        $templateProcessor->setValue('hora_prueba', $hora_prueba);
        $templateProcessor->setValue('hora_realizacion', $hora_realizacion);
        $templateProcessor->setValue('fecha_realizacion', $fecha_realizacion);
        $templateProcessor->setValue('opcion_titulacion', $opcion_titulacion);
        $templateProcessor->setValue('carrera', $carrera);
        $templateProcessor->setValue('num_control', $num_control);
        $templateProcessor->setValue('alumno', $alumno);

        if (!\File::exists(storage_path('app/public/nombramientos/') . $nombramiento->id)) {
            \File::makeDirectory(storage_path('app/public/nombramientos/') . $nombramiento->id);
        }

        $templateProcessor->saveAs(storage_path('app/public/nombramientos/') . $nombramiento->id . '/solicitud_intervencion_sustentante.docx');

        return response()->download(storage_path('app/public/nombramientos/') . $nombramiento->id . '/solicitud_intervencion_sustentante.docx');
    }
}
