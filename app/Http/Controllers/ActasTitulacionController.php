<?php


namespace Titulaciones\Http\Controllers;


use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Titulaciones\Exports\ActasExports;
use Titulaciones\Exports\NombramientosExports;

class ActasTitulacionController extends Controller
{
    public function download()
    {
        $del = \Request::query('del', Carbon::now()->format('Y-m-d'));
        $al  = \Request::query('al', Carbon::now()->format('Y-m-d'));

        return Excel::download(new ActasExports($del, $al), 'actas_titulacion.xlsx');
    }
}