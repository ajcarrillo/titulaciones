<?php

namespace Titulaciones\Http\Controllers\Auth;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Titulaciones\Http\Controllers\Controller;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function authenticated($request, $user)
    {
        if ($user->hasRole('division') or $user->hasRole('jefe_depto')) {
            return redirect()->to('/titulaciones');
        }

        if ($user->hasRole('servicios_escolares')) {
            return redirect()->route('servicios.escolares.');
        }

        return redirect($this->redirectTo);
    }


}
