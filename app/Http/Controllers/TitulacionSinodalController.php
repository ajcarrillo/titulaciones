<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 11/10/20
 * Time: 21:44
 */

namespace Titulaciones\Http\Controllers;


use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Titulaciones\Models\Maestro;
use Titulaciones\Models\Nombramiento;

class TitulacionSinodalController extends Controller
{
    protected $root = "Titulaciones/Tramites/Sinodales";

    public function create(Nombramiento $titulacion)
    {
        $titulacion->load('alumno');

        return Inertia::render("{$this->root}/Create", [
            'titulacion'   => $titulacion,
            'cat_maestros' => $this->getMaestros(),
        ]);
    }

    private function getMaestros()
    {
        return Maestro::query()
            ->orderBy('nombre_completo')
            ->selectRaw('id as value, nombre_completo as label')
            ->get();
    }

    public function store(Request $request, Nombramiento $titulacion)
    {
        try {
            DB::transaction(function () use ($request, $titulacion) {
                $presi         = $request->post('presidente');
                $secretario    = $request->post('secretario');
                $vocal         = $request->post('vocal');
                $vocalSuplente = $request->post('vocalSuplente');

                if (!is_null($presi)) {
                    $titulacion->sinodales()->updateOrCreate(
                        [ 'tipo' => 'PRESIDENTE' ], [ 'maestro_id' => $presi ]
                    );
                }

                if (!is_null($secretario)) {
                    $titulacion->sinodales()->updateOrCreate(
                        [ 'tipo' => 'SECRETARIO' ], [ 'maestro_id' => $secretario ]
                    );
                }

                if (!is_null($vocal)) {
                    $titulacion->sinodales()->updateOrCreate(
                        [ 'tipo' => 'VOCAL' ], [ 'maestro_id' => $vocal ]
                    );
                }

                if (!is_null($vocalSuplente)) {
                    $titulacion->sinodales()->updateOrCreate(
                        [ 'tipo' => 'VOCAL SUPLENTE' ], [ 'maestro_id' => $vocalSuplente ]
                    );
                }
            });

            return Redirect::route('titulaciones.index')->with('success', 'Los sinodales se asignaron correctamente');
        } catch (\Exception $e) {
            return Redirect::back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return Redirect::back()->with('error', $e->getMessage());
        }
    }

    public function edit(Nombramiento $titulacion)
    {
        $titulacion->load('sinodales', 'sinodales.maestro');

        return Inertia::render("{$this->root}/Edit", [
            'titulacion'   => $titulacion,
            'cat_maestros' => $this->getMaestros(),
        ]);
    }
}
