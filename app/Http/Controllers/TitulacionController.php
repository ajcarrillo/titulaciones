<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 11/10/20
 * Time: 0:39
 */

namespace Titulaciones\Http\Controllers;


use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Titulaciones\Models\Filters\NombramientoFilters;
use Titulaciones\Models\Generacion;
use Titulaciones\Models\Horario;
use Titulaciones\Models\Nombramiento;
use Titulaciones\Models\NumeroOficio;

class TitulacionController extends Controller
{
    private $root = 'Titulaciones/Tramites';

    public function index(Request $request, NombramientoFilters $filters)
    {
        $nombramientos = $this->getNombramientos($request, $filters);
        $title         = auth()->user()->hasRole('titulacion') ? 'Solicitudes' : 'Nombramientos';
        $filters       = $request->only('search', 'status', 'year');
        $status        = $this->getStatus();

        return Inertia::render("{$this->root}/Index", [
            'nombramientos' => $nombramientos,
            'title'         => $title,
            'filters'       => $filters,
            'status'        => $status,
        ]);
    }

    public function create()
    {
        return Inertia::render("{$this->root}/Create");
    }

    public function edit(Nombramiento $titulacion)
    {
        $titulacion->load('opcion', 'modulo', 'horario', 'alumno', 'alumno.carrera');

        return Inertia::render("{$this->root}/Edit", [
            'nombramiento' => $titulacion,
            'opciones'     => $this->getOpciones($titulacion->alumno->anio_ingreso),
            'horarios'     => $this->getHorarios($titulacion->fecha, $titulacion->id),
        ]);
    }

    public function update(Request $request, Nombramiento $titulacion)
    {
        $titulacion->update([
            'horario_id'             => $request->input('horario_id'),
            'fecha'                  => $request->input('fecha'),
            'proyecto'               => $request->input('proyecto'),
            'modulo_id'              => $request->input('modulo_id') === "null" ? NULL : $request->input('modulo_id'),
            'opcion_id'              => $request->input('opcion_id'),
            'representante_division' => $request->input('representante_division'),
            'video_link'             => $request->input('video_link'),
        ]);

        return Redirect::back()->with('success', 'Solicitud actualizada.');
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();

            $data   = $request->input('nombramiento');
            $oficio = NumeroOficio::getNumeroOficio();
            $oficio->update([ 'activo' => 0 ]);

            $nombramiento                 = new Nombramiento($data);
            $nombramiento->estatus        = 'P';
            $nombramiento->numero_memo_id = $oficio->id;
            $nombramiento->save();

            DB::commit();

            return Redirect::route('titulaciones.index')->with('success', 'Solicitud creada.');
        } catch (\Exception $e) {
            DB::rollBack();

            return unprocessable_entity([ $e->getMessage() ]);
        }
    }

    private function getNombramientos(Request $request, NombramientoFilters $filters)
    {
        $data = $request->only('search', 'status', 'page', 'year');

        $query = Nombramiento::query()
            ->with('sinodales', 'sinodales.maestro', 'memo', 'alumno', 'alumno.carrera', 'opcion', 'modulo', 'horario')
            ->filterBy($filters, $data);

        if (auth()->user()->hasRole('jefe_depto')) {
            $query->whereHas('alumno', function ($alumno) {
                $alumno->whereIn('carrera_id', auth()->user()->departamento->carreras()->pluck('id'));
            })->whereIn('estatus', [ 'E', 'C' ]);
        }

        return $query->orderBy('fecha', 'desc')->paginate()->appends($data);
    }

    private function getStatus()
    {
        if (auth()->user()->hasRole('jefe_depto')) {
            return [
                [
                    'label' => 'Pendiente',
                    'value' => 'E',
                ],
                [
                    'label' => 'Concluido',
                    'value' => 'C',
                ],
            ];
        }

        return [
            [
                'label' => 'Pendiente',
                'value' => 'P',
            ],
            [
                'label' => 'Enviado',
                'value' => 'E',
            ],
            [
                'label' => 'Concluido',
                'value' => 'C',
            ],
        ];
    }

    private function getOpciones($year)
    {
        $query = Generacion::with([ 'opciones' => function ($query) {
            $query->selectRaw('opciones_titulacion.id, concat_ws(" ", opciones_titulacion.clave, opciones_titulacion.descripcion) as text')
                ->orderBy('clave');
        } ]);

        $generacion = $query->whereRaw($year . " BETWEEN inicio and fin")
            ->first();

        return $generacion->opciones()
            ->with('modulos')
            ->get();
    }

    private function getHorarios($date, $exclude)
    {
        $nombramientos = Nombramiento::query()
            ->where('fecha', $date)
            ->where('id', '!=', $exclude)
            ->pluck('horario_id');

        return Horario::query()
            ->selectRaw('id as value, SUBSTR(hora,1,5) as text')
            ->get();
    }
}
