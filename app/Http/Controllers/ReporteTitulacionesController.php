<?php

namespace Titulaciones\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Titulaciones\Exports\NombramientosExports;

class ReporteTitulacionesController extends Controller
{
    public function download()
    {
        $del = \Request::query('del', Carbon::now()->format('Y-m-d'));
        $al  = \Request::query('al', Carbon::now()->format('Y-m-d'));

        return Excel::download(new NombramientosExports($del, $al), 'titulaciones.xlsx');
    }
}
