<?php

namespace Titulaciones\Http\Controllers;

use Carbon\Carbon;
use Complex\Exception;
use DB;
use Illuminate\Http\Request;
use Titulaciones\Models\NumeroOficio;

class NumeroDeOficioController extends Controller
{
    public function index(Request $request)
    {
        $year = Carbon::now()->year;

        if ($request->filled('year')) {
            $year = $request->get('year');
        }

        $query = NumeroOficio::with('nombramiento', 'nombramiento.alumno')
            ->where('year', 'like', '%' . $year . '%')
            ->raw('ORDER BY LENGTH(numero_oficio), numero_oficio');

        if ($request->filled('numero_oficio')) {
            $query->where('numero_oficio', 'like', '%' . $request->query('numero_oficio') . '%');
        }

        $oficios = $query->paginate(50)->appends($request->all());

        return view('oficios.index', compact('year', 'oficios'));
    }

    public function store(Request $request)
    {
        try {

            /*if (NumeroOficio::where('year', $request->input('new_year'))->count()) {
                throw new Exception('Ya existe al menos un número de oficio del año: ' . $request->input('new_year'));
            }*/

            DB::transaction(function () use ($request) {
                for ($i = $request->input('de'); $i <= $request->input('hasta'); $i++) {
                    NumeroOficio::create([
                        'numero_oficio' => $i,
                        'activo'        => true,
                        'year'          => $request->input('new_year'),
                    ]);
                }
            });
            flash('Los números de oficio se generaron correctamente')->success();

            return back();
        } catch (\Exception $e) {
            flash($e->getMessage())->error();
            \Log::error($e->getMessage());

            return back();
        } catch (\Throwable $e) {
            flash('Ha ocurrido un error intenta de nuevo')->error();
            \Log::error($e->getMessage());

            return back();
        }
    }
}
