<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 18/09/20
 * Time: 14:18
 */

namespace Titulaciones\Traits;


use Titulaciones\Classes\QueryFilter;

trait FilterBy
{
    public function scopeFilterBy($query, QueryFilter $filters, array $data)
    {
        return $filters->applyTo($query, $data);
    }
}
