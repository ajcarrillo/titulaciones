<?php
/**
 * Created by PhpStorm.
 * User: andres
 * Date: 13/02/21
 * Time: 13:10
 */

namespace Titulaciones\Traits;


trait ExportToCSV
{
    public function outputCSVStatement($items)
    {
        header("content-type:application/csv;charset=UTF-8");
        header("Content-Disposition: attachment; filename=export-" . date("YmdHis") . ".csv");
        header("Pragma: no-cache");
        header("Expires: 0");

        $output = fopen("php://output", "w");

        $headers = array_keys(current($items));
        fputcsv($output, $headers);

        foreach($items as $values){
            fputcsv($output, $values);
        }

        mb_convert_encoding($output, 'UTF-16LE', 'UTF-8');

        fclose($output);
        exit();
    }
}
