<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToSolicitudesConvalidacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('solicitudes_convalidacion', function (Blueprint $table) {
            $table->tinyInteger('status_id')
                ->after('institucion_destino_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('solicitudes_convalidacion', function (Blueprint $table) {
            $table->dropColumn('status_id');
        });
    }
}
