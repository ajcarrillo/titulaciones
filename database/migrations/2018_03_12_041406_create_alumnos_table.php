<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAlumnosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('alumnos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_control', 10)->index();
            $table->string('nombre', 420)->index();
            $table->string('apellido', 420)->index();
            $table->string('nombre_apellido', 840)->index();
            $table->string('sexo', 1)->index()->nullable();
            $table->year('anio_ingreso')->index()->nullable();
            $table->string('periodo_ingreso', 1)->index()->nullable();
            $table->unsignedInteger('carrera_id')->index()->nullable();
            $table->foreign('carrera_id')->references('id')->on('carreras');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alumnos');
    }
}
