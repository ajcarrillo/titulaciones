<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConvalidacionesAsignaturasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convalidaciones_asignaturas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('convalidacion_carrera_id');
            $table->foreign('convalidacion_carrera_id')->references('id')->on('convalidaciones_carreras');
            $table->text('asignaturas_cursadas');
            $table->unsignedBigInteger('asignatura_convalidar_id');
            $table->foreign('asignatura_convalidar_id')->references('id')->on('asignaturas');
            $table->tinyInteger('porcentaje');
            $table->unsignedInteger('departamento_id')->nullable();
            $table->foreign('departamento_id')->references('id')->on('jefes_departamentos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convalidaciones_asignaturas');
    }
}
