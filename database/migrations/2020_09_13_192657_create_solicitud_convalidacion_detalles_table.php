<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudConvalidacionDetallesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitud_convalidacion_detalles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('solicitud_convalidacion_id');
            $table->foreign('solicitud_convalidacion_id', 'scd_sc_id_foreign')->references('id')->on('solicitudes_convalidacion');
            $table->unsignedBigInteger('asignatura_id');
            $table->foreign('asignatura_id', 'scd_a_id_foreign')->references('id')->on('asignaturas');
            $table->string('calificacion');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitud_convalidacion_detalles');
    }
}
