<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConvalidacionesCarrerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('convalidaciones_carreras', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('carrera_origen_id');
            $table->foreign('carrera_origen_id')->references('id')->on('carreras');
            $table->unsignedInteger('carrera_destino_id');
            $table->foreign('carrera_destino_id')->references('id')->on('carreras');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('convalidaciones_carreras');
    }
}
