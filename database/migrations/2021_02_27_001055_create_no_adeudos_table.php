<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNoAdeudosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('no_adeudos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('alumno_id');
            $table->foreign('alumno_id')->references('id')->on('alumnos');
            $table->text('observaciones')->nullable();
            $table->boolean('adeuda_biblioteca')->nullable();
            $table->boolean('adeuda_financieros')->nullable();
            $table->boolean('adeuda_extraescolares')->nullable();
            $table->boolean('adeuda_deptoacademico')->nullable();
            $table->boolean('adeuda_divisionestudios')->nullable();
            $table->boolean('adeuda_gestionvinculacion')->nullable();
            $table->string('observacion_biblioteca')->nullable();
            $table->string('observacion_financieros')->nullable();
            $table->string('observacion_extraescolares')->nullable();
            $table->string('observacion_deptoacademico')->nullable();
            $table->string('observacion_divisionestudios')->nullable();
            $table->string('observacion_gestionvinculacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('no_adeudos');
    }
}
