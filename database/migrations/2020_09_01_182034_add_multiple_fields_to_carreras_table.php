<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMultipleFieldsToCarrerasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carreras', function (Blueprint $table) {
            $table->string('sii_id', 3)
                ->after('id')
                ->nullable();
            $table->tinyInteger('reticula')
                ->after('jefe_departamento_id')
                ->nullable();
            $table->string('nivel_escolar', 1)
                ->after('jefe_departamento_id')
                ->nullable();
            $table->string('tipo', 2)
                ->after('jefe_departamento_id')
                ->nullable();
            $table->string('estado')
                ->after('jefe_departamento_id')
                ->nullable();
            $table->string('clave_oficial')
                ->after('jefe_departamento_id')
                ->nullable();

            $table->index('sii_id');
            $table->index('reticula');
            $table->index('clave_oficial');
            $table->unique([ 'sii_id', 'reticula' ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carreras', function (Blueprint $table) {
            $table->dropIndex([ 'sii_id' ]);
            $table->dropIndex([ 'reticula' ]);
            $table->dropIndex([ 'clave_oficial' ]);
            $table->dropUnique([ 'sii_id', 'reticula' ]);

            $table->dropColumn('sii_id');
            $table->dropColumn('reticula');
            $table->dropColumn('nivel_escolar');
            $table->dropColumn('tipo');
            $table->dropColumn('estado');
            $table->dropColumn('clave_oficial');
        });
    }
}
