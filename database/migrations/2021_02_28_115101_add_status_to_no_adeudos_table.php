<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStatusToNoAdeudosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('no_adeudos', function (Blueprint $table) {
            $table->string('estatus')
                ->nullable()
                ->after('observacion_gestionvinculacion')
                ->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('no_adeudos', function (Blueprint $table) {
            $table->dropIndex(['estatus']);
            $table->dropColumn('estatus');
        });
    }
}
