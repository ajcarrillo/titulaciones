<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSolicitudesConvalidacionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('solicitudes_convalidacion', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('alumno_id');
            $table->foreign('alumno_id')->references('id')->on('alumnos');
            $table->unsignedInteger('carrera_destino_id');
            $table->foreign('carrera_destino_id')->references('id')->on('carreras');
            $table->unsignedBigInteger('institucion_origen_id');
            $table->foreign('institucion_origen_id')->references('id')->on('instituciones');
            $table->unsignedBigInteger('institucion_destino_id');
            $table->foreign('institucion_destino_id')->references('id')->on('instituciones');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('solicitudes_convalidacion');
    }
}
