let mix = require("laravel-mix");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.webpackConfig({
  resolve: {
    alias: {
      "@": __dirname + "/resources/assets/js"
    },
  },
  output: {
    chunkFilename: "[name].js?id=[chunkhash]",
  }
});

mix.js("resources/assets/js/app.js", "public/js")
  .js("resources/assets/js/servicios_escolares.js", "public/js")
  .js("resources/assets/js/base.js", "public/js")
  .js("resources/assets/js/app_inertia.js", "public/js")
  .sass("resources/assets/sass/app.scss", "public/css")
  .copy("resources/assets/img/logo_tec.png", "public/img/logo_tec.png")
  .browserSync({
    proxy: "titulaciones.test",
    files: [
      "resources/**/*",
    ]
  });


mix.sass("app/Modules/Convalidaciones/Resources/assets/sass/app.scss", "public/modules/convalidaciones/css/convalidaciones.css")
  .js("app/Modules/Convalidaciones/Resources/assets/js/app.js", "public/modules/convalidaciones/js/convalidaciones.js")

mix.options({
  terser: {
    extractComments: false,
  }
})

if (mix.inProduction()) {
  mix.version();
}
